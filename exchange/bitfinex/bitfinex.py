from exchange.lib_exchange import Exchange
from exchange.exchange_id import ExchangeId
from util.lib_log import Log
from util.xch_msg import XchTransactionMsg, XchMovementMsg
import time
import json
import base64
import hmac
import hashlib
import requests
import re


class Bitfinex(Exchange):

    def __init__(self, api_config):
        super(Bitfinex, self).__init__(ExchangeId.Bitfinex)
        self.__key = api_config.key
        self.__secret = api_config.secret
        self._url = 'https://api.bitfinex.com/v1/'
        # self.api_config_label = api_config.label

    def perform(self, api, params=None):
        if not params:
            params = {}
        response = {}
        retries = 10
        params['dev'] = 'WFKCNLWKNKLWN'

        error = None
        for i in range(0, retries):
            try:
                # r = requests.get(self._url + api)
                # if r.status_code != 200:
                #     raise Exchange.RemoteError(r.content)
                resp = requests.get(self._url + api, params=params)
                response['data'] = resp.json()
                response['status'] = 'ok'
                return response
            except Exception as e:
                if i < retries - 1:
                    time.sleep(i * 0.5)
                    continue
                response['status'] = 'error'
                response['error'] = e.message
                response['data'] = {}
                return response

        return response

    def perform_private(self, api, params=None):
        if not params:
            params = {}
        params['dev'] = 'WFKCNLWKNKLWN'
        retries = 20
        response = {}

        nonce = (time.time() * 100000)

        payload_object = {
            'request': '/v1/' + api,
            'nonce': '%i' % nonce,  # convert to string
            'options': {}
        }
        for key, value in params.items():
            payload_object[key] = value

        payload_json = json.dumps(payload_object)

        payload = base64.urlsafe_b64encode(bytes(payload_json, 'utf-8'))

        m = hmac.new(bytes(self.__secret, 'utf-8'), payload, hashlib.sha384)
        m = m.hexdigest()

        # headers
        headers = {
            'X-BFX-APIKEY': self.__key,
            'X-BFX-PAYLOAD': payload,
            'X-BFX-SIGNATURE': m
        }
        read_timeout = 15.0
        if api == 'history/movements':
            read_timeout = 60.0

        for i in range(retries):
            try:
                r = requests.post(self._url + api, data={}, headers=headers, timeout=(5.0, read_timeout))

                data = r.json()

                if isinstance(data, dict):
                    if r.status_code >= 400:
                        message = str(data.get('message'))
                        Log.get_instance().debug('message: "%s"' % message)

                        Log.get_instance().warning('Error %i message: "%s" Data: %s' %
                                                       (r.status_code, message, str(data)))

                if r.status_code != 200:
                    raise Exception("Remote Error" + r.content)

                return {
                    'status': 'ok',
                    'data': data
                }

            except Exception as e:
                if i < retries - 1:
                    time.sleep(i * 0.5)
                    continue

                response = response or {}
                response['status'] = 'error'
                response['error'] = e.message
                response['data'] = {}
                return response

        return response

    @staticmethod
    def _check_currency(currency):
        return 'DSH' if currency.upper() == 'DRK' else currency.upper()

    def get_balance_history(self, currency, time_start=None, time_end=None):
        # time_end = time_end or time.time() + 1
        # time_start, time_end = min(time_start, time_end), max(time_start, time_end)
        base = self._check_currency(currency)

        params = {'currency': base, 'limit': 500}
        if time_start:
            params['since'] = time_start
        if time_end:
            params['until'] = time_end
        data = self.perform_private('history', params)
        return data

    def __convert_transaction(self, transaction):
        """
        @param transaction:
        @return:
        """

        result = XchTransactionMsg(

            tid=str(transaction['tid']),
            fee_cur_name=str(transaction['fee_currency']),
            fee_amount=float(transaction['fee_amount']),
            oid=str(transaction['order_id']),
            price=float(transaction['price']),
            amount=float(transaction['amount']),
            type=transaction['type'].upper(),
            timestamp=float(transaction['timestamp'])
        )

        return result

    def get_trade_history(self, base_currency, quote_currency, time_limit=None, end_time=None, count=500):
        base = self._check_currency(base_currency)
        quote = self._check_currency(quote_currency)
        if not end_time:
            end_time = time.time()
        if not time_limit:
            time_limit = end_time - 7 * 86400.0
        symbol = base + quote
        temp_time_limit = time_limit

        params = {
            'symbol': symbol,
            'timestamp': str(int(temp_time_limit)),
            'limit_trades': count,
            'until': str(int(end_time))
        }

        runs = 0
        data = []
        while temp_time_limit >= time_limit:
            runs += 1
            if runs > 50:
                break
            tmp = self.perform_private('mytrades', params)
            if not tmp or tmp['status'] != 'ok':
                raise Exception('Could not fetch trades')
            data_added = tmp['data']
            data += data_added
            if len(data_added) < count:
                # Terminate if the API does not return more entries until that timestamp
                break
            else:
                temp_time_limit = data[-1]['timestamp']
                params['until'] = temp_time_limit

        data = map(self.__convert_transaction, data)
        return data

    def __convert_order(self, order):
        """
        Input
        symbol (string): The symbol name the order belongs to.
        exchange (string): "bitfinex", "bitstamp".
        price (decimal): The price the order was issued at (can be null for market orders).
        avg_execution_price (decimal): The average price at which this order as been executed so far. 0 if the order has not been executed at all. side (string): Either "buy" or "sell".
        type (string): Either "market" / "limit" / "stop" / "trailing-stop".
        timestamp (time): The timestamp the order was submitted.
        is_live (bool): Could the order still be filled?
        is_cancelled (bool): Has the order been cancelled?
        was_forced (bool): For margin only: true if it was forced by the system.
        executed_amount (decimal): How much of the order has been executed so far in its history?
        remaining_amount (decimal): How much is still remaining to be submitted?
        original_amount (decimal): What was the order originally submitted for?


        out_model = {
            'data': 'dict',
            'oid': 'str',
            'price': 'float',
            'amount': 'float',
            'type': 'str|buy|sell',
            'time': 'float',
            'status': 'str'
        }
        @param order:
        @return:
        """

        try:

            result = {
                'data': order,
                'oid': str(order['id']),
                'price': float(order['price']),
                'amount': float(order['executed_amount']),
                'type': order['side'],
                'time': float(order['timestamp']),
                'status': 'open'
            }
            if order['is_cancelled']:
                result['status'] = 'canceled'
            if result['amount'] == 0 and order['executed_amount'] == order['original_amount']:
                result['status'] = 'executed'
            return result
        except Exception as e:
            log = Log.get_instance()
            log.error(e)
            log.warning('Failed to convert order')
            log.warning(order)

    def get_orders_history(self, time_limit=None, end_time=None, count=500):
        if not end_time:
            end_time = time.time()
        if not time_limit:
            time_limit = end_time - 7 * 86400.0

        params = {
            'limit': count
        }
        response = None
        for tries in range(0, 3):
            try:
                response = self.perform_private('orders/hist')
                if response is None:
                    pass

                response['orders'] = map(self.__convert_order, response['data'])
                response['orders'] = filter(None, response['orders'])
                response['orders'] = [o for o in response['orders'] if time_limit < o.timestamp < end_time]
            except:
                continue
            break

        return response

    def get_deposits_and_withdraws(self, currency, time_limit=None, end_time=None, count=500):
        base = 'DSH' if currency == 'DRK' else currency
        if not end_time:
            end_time = time.time()
        if not time_limit:
            time_limit = end_time - 7 * 86400.0
        temp_time_limit = time_limit

        params = {
            'currency': base,
            'since': str(int(temp_time_limit)),
            'limit': count,
            'until': str(int(end_time))
        }
        runs = 0
        data = []
        while temp_time_limit >= time_limit:
            runs += 1
            if runs > 50:
                break
            tmp = self.perform_private('history/movements', params)
            if not tmp or tmp['status'] != 'ok':
                raise Exception('Could not fetch movements')
            data_added = tmp['data']
            data += data_added
            if len(data_added) < count:
                # Terminate if the API does not return more entries until that timestamp
                break
            else:
                temp_time_limit = data[-1]['timestamp']
                params['until'] = temp_time_limit

        data = map(self.filter_movement_result, data)
        data = filter(None, data)
        result = {
            'status': 'ok',
            'data': list(data)
        }
        return result

    def filter_movement_result(self, data):

        currency = data[u'currency'].upper()
        if currency == 'DSH':
            currency = 'DRK'

        fee = data.get('fee', None)
        movement = XchMovementMsg(
            id=str(data[u'id']),
            exchange_id=self.exchange_id,
            currency=currency,
            timestamp=float(data['timestamp']),
            type=data['type'].upper(),
            amount=float(data['amount']),
            description=data['description'],
            target_address=data.get('address', None),
            transaction_id=data.get('txid', None),
            fee=float(fee) if fee else fee
        )

        if not movement.transaction_id or len(movement.transaction_id) < 64:
            status = data[u'status']
            if movement.type == u'WITHDRAWAL':
                txid = None

                if status == u'PENDING APPROVAL':
                    return
                if status == u'CANCELED':
                    return
                if u'txid:' in movement.description:
                    try:
                        txids = movement.description.split(', txid: ')
                        movement.target_address = txids[0]
                        if len(txids) > 2:
                            txid = ','.join(txids[1:])[:128]
                        else:
                            txid = txids[1]
                    except ValueError:
                        # u'0x9e6316f44baeeee5d41a1070516cc5fa47baf227,
                        # txid: 0x0ccb9cde190e513dc5ad513d15bd6479266e299c1e533d0ef686b1ee42aa06f1,
                        # txid: 0x5d2d29a011fe042f76beb22b495df5391dfcd6b7e080116b0b70b35a1b74ba01'
                        pass
                if not txid:
                    txid = movement.id
                movement.transaction_id = txid

            elif movement.type == u'DEPOSIT':

                if status == u'ZEROCONFIRMED':
                    # @todo: think about if this needs handling
                    return
                elif status == u'CANCELED':
                    return
                elif status == u'COMPLETED':
                    movement.transaction_id = movement.description
                    # movement.lookup_transaction()
                else:
                    raise ValueError('Invalid Status %s' % status)
            else:
                raise ValueError('Invalid Transaction Type %s' % movement.type)

        if movement.transaction_id and movement.transaction_id.startswith(u'Deposit to old address'):
            # Invalidate this transaction, as it cannot be parsed later. WTF Bitfinex??? What are you doing???
            return

        if movement.target_address and len(movement.target_address) < 32:
            movement.target_address = None
        if not movement.target_address and movement.type == u'WITHDRAWAL':
            target = re.findall(r'\bBANK: \b[\w\s]+,', movement.description, re.I)
            if target:
                movement.target_address = target[0].replace('Bank: ', '')[:-1]
        # print movement.to_dict()
        return movement

    def get_balances(self):
        data = self.perform_private('balances')
        return data

    def get_account_info(self):
        data = self.perform_private('account_infos')
        return data

    def get_summary(self):
        data = self.perform_private('summary')
        return data

    def get_traded_currencies(self):
        data = self.get_summary()
        currencies = []
        for val in data['data'].get('trade_vol_30d', []):
            if float(val.get('vol', 0)) > 0:
                if 'USD' in val['curr'].upper():
                    continue
                currencies.append(val['curr'])
        return currencies

    def get_ticker(self, base_cur, quote_cur):
        symbol = base_cur+quote_cur
        tick = self.perform('pubticker/'+symbol.lower())
        return tick

    def get_wallet_balance(self):
        balances = self.get_balances()
        total_balances = dict(total=0, cash=0, coins_value=0)
        for bal in balances['data']:
            if bal['currency'].lower() == 'usd':
                total_balances['cash'] += float(bal['amount'])
            else:
                tick = self.get_ticker(bal['currency'], 'usd')
                total_balances['coins_value'] += float(bal['amount']) * float(tick['data']['last_price'])
        total_balances['total'] = total_balances['cash'] + total_balances['coins_value']
        return total_balances
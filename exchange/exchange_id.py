

class ExchangeId(object):
    """
    Exchange ids class that is used to normalize names used for exchanges.
    It is recommended to use this utility class each time to pass an exchange Id inside a code
    """
    Poloniex = 'Poloniex'
    Bitstamp = 'Bitstamp'
    Kraken = 'Kraken'
    Bitfinex = 'Bitfinex'

    ALL = [
        Kraken,
        Bitstamp,
        Bitfinex,
        Poloniex
    ]

    @classmethod
    def validate(cls, exchange_id):
        return exchange_id in cls.ALL

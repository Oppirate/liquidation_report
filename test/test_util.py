import unittest
from util.lib_util import Util


class TestUtil(unittest.TestCase):

    def setUp(self):
        pass

    def test_send_email(self):
        send_from = 'seifallah.kouki@bcc.gmbh'
        file = 'reports/Hive Liquidation Report.xlsx'
        response = Util().send_mail(send_from, ['seifallah.kouki@genesis-mining.com', 'seifallah.kouki@blockchain-consulting.net'], 'Liquidation Report', 'Report is attached',
                       file)
        self.assertEqual(response['status'], 'ok')


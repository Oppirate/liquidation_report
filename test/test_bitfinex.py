import unittest
import time
from exchange.bitfinex.bitfinex import Bitfinex
from util.lib_config import from_file
from util.xch_msg import XchTransactionMsg, XchMovementMsg


class TestBitfinex(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        api_config = from_file('api/bitfinex')
        cls._exchange = Bitfinex(api_config)

    def test_balance_history(self):
        now = time.time()
        data = self._exchange.get_balance_history('BTC', time_end=str(now), time_start=str(now-60*86400))
        print(data)
        self.assertIsInstance(data, dict)
        self.assertEqual(data.get('status'), 'ok')

    def test_trade_history(self):
        for base, quote in [('ETC', 'USD'), ('ETC', 'BTC'), ('ETH', 'USD'), ('ETH', 'BTC')]:
            data = self._exchange.get_trade_history(base, quote)
            self.assertIsInstance(data, map)
            try:
                first_element = next(data)
                print(first_element)
                self.assertIsInstance(first_element, XchTransactionMsg)
            except StopIteration:
                pass
            time.sleep(1)

    def test_order_history(self):
        data = self._exchange.get_orders_history()
        self.assertIsInstance(data, dict)
        self.assertEqual(data.get('status'), 'ok')
        orders = data.get('orders')
        if orders:
            self.assertIsInstance(orders[0], dict)

    def test_deposit_withdrawals(self):
        data = self._exchange.get_deposits_and_withdraws('USD')
        self.assertIsInstance(data, dict)
        self.assertEqual(data.get('status'), 'ok')
        withdraws = data.get('data')
        if withdraws:
            self.assertIsInstance(withdraws[0], XchMovementMsg)
            print(withdraws[0].to_dict())

        data = self._exchange.get_deposits_and_withdraws('ETH')
        self.assertIsInstance(data, dict)
        self.assertEqual(data.get('status'), 'ok')
        deposits = data.get('data')
        if deposits:
            self.assertIsInstance(deposits[0], XchMovementMsg)
            print(withdraws[0].to_dict())

    def test_account_info(self):
        data = self._exchange.get_account_info()
        print(data)
        self.assertIsInstance(data, dict)
        self.assertEqual(data.get('status'), 'ok')

    def test_summary(self):
        data = self._exchange.get_summary()
        print(data)
        self.assertIsInstance(data, dict)
        self.assertEqual(data.get('status'), 'ok')

    def test_traded_currencies(self):
        data = self._exchange.get_traded_currencies()
        print(data)
        self.assertIsInstance(data, dict)
        self.assertEqual(data.get('status'), 'ok')

    def test_wallet_balances(self):
        data = self._exchange.get_balances()
        print(data)
        self.assertIsInstance(data, dict)
        self.assertEqual(data.get('status'), 'ok')

    def test_ticker(self):
        data = self._exchange.get_ticker('btc', 'usd')
        print(data)
        self.assertIsInstance(data, dict)
        self.assertEqual(data.get('status'), 'ok')

    def test_total_balance(self):
        data = self._exchange.get_wallet_balance()
        print(data)
        self.assertIsInstance(data, dict)
        self.assertEqual(data.get('status'), 'ok')


if __name__ == '__main__':
    unittest.main()
import unittest
import datetime

from spreadsheet.report_writer import ReportWriter


class TestReportWriter(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls._writer = ReportWriter()
        cls._writer.bootstrap_exchanges()

    @classmethod
    def tearDownClass(cls):
        cls._writer.api.delete_spreadsheet()

    def test_trades_updates(self):
        ss = self._writer.spreadsheet
        self._writer.initiate_default_sheets()
        self._writer.update_trades_sheet()
        print('success')

    def test_withdrawal_updates(self):
        ss = self._writer.spreadsheet
        # make longer time interval
        temp_start = self._writer.start
        self._writer.start = self._writer.end - datetime.timedelta(days=30)
        self._writer.initiate_default_sheets()
        self._writer.update_withdrawal_sheet()
        print('success')
        self._writer.start = temp_start

    def test_aggregate_trades_to_orders(self):
        trades = []
        reports = []
        for xch_id, xch in self._writer.exchanges.items():
            xch_trades, xch_report = self._writer.get_exchange_trades(xch)
            trades += xch_trades
            reports += xch_report
        print(trades)

    def test_withdrawals(self):
        import datetime
        # make longer time interval
        temp_start = self._writer.start
        self._writer.start = self._writer.end - datetime.timedelta(days=30)
        deps = []
        withds = []

        for xch_id, xch in self._writer.exchanges.items():
            deposits, withdrawals = self._writer.get_exchange_withdrawals(xch)
            deps += deposits
            withds += withdrawals
        print(withds)
        self._writer.start = temp_start

    def test_coins_sent_to_ledger(self):
        coins_sent_to_ledger = []
        reports = []
        for coin, address in self._writer._ledger_addresses.items():
            coin_sent_to_ledger, report = self._writer.get_coin_sent_to_ledger(coin, address)
            coins_sent_to_ledger += coin_sent_to_ledger
            reports.append(report)
        print('Success')

    def test_coins_inventory_updates(self):
        ss = self._writer.spreadsheet
        import datetime
        # make longer time interval
        temp_start = self._writer.start
        self._writer.start = self._writer.end - datetime.timedelta(days=30)
        self._writer.initiate_default_sheets()
        self._writer.update_inventory_sheet()
        print('success')
        self._writer.start = temp_start

    def test_management_overview_updates(self):
        ss = self._writer.spreadsheet
        import datetime
        # make longer time interval
        temp_start = self._writer.start
        self._writer.start = self._writer.end - datetime.timedelta(days=30)
        self._writer.initiate_default_sheets()
        self._writer._management_overview_reports = dict(
            ledger_reports_out=[
                dict([('Coin', 'ETH'), ('Ledger Address', '0xbalabala'), ('Number of Transactions', 10), ('Total Amount of Coins Sent', 12)]),
                dict([('Coin', 'ETH'), ('Ledger Address', '0xbalabala1'), ('Number of Transactions', 3),
                      ('Total Amount of Coins Sent', 10.)]),
                dict([('Coin', 'ETH'), ('Ledger Address', '0xbalabala2'), ('Number of Transactions', 1),
                      ('Total Amount of Coins Sent', 1.2)]),
                dict([('Coin', 'ETC'), ('Ledger Address', '0xbalabalaetc'), ('Number of Transactions', 5),
                      ('Total Amount of Coins Sent', 12.11)])
            ],
            deposit_reports=[
                dict([('Exchange', 'Bitfinex'), ('Coin', 'ETH'), ('Sender Address', '0xblavlabla'),
                      ('Number of Transactions', 12), ('Total Amount Received', 14.19)]),
                dict([('Exchange', 'Bitfinex'), ('Coin', 'ETH'), ('Sender Address', '0xblavlabla1'),
                      ('Number of Transactions', 2), ('Total Amount Received', 19.01)]),
                dict([('Exchange', 'Bitfinex'), ('Coin', 'ETC'), ('Sender Address', '0xblavlablaetc'),
                      ('Number of Transactions', 102), ('Total Amount Received', 140.19)]),
                dict([('Exchange', 'Kraken'), ('Coin', 'ETH'), ('Sender Address', '0xblavlabla'),
                      ('Number of Transactions', 12), ('Total Amount Received', 14.19)])
            ],
            trade_reports=[
                dict([('Exchange', 'Bitfinex'), ('Symbol', 'BTCUSD'), ('Total Liquidated Amount', 15),
                      ('Average Liquidated Price', 6521), ('Total Liquidated Value', 97815)]),
                dict([('Exchange', 'Bitfinex'), ('Symbol', 'ETHUSD'), ('Total Liquidated Amount', 105),
                      ('Average Liquidated Price', 651), ('Total Liquidated Value', 97015)]),
                dict([('Exchange', 'Bitfinex'), ('Symbol', 'ETCUSD'), ('Total Liquidated Amount', 1500),
                      ('Average Liquidated Price', 65.21), ('Total Liquidated Value', 970015)]),
                dict([('Exchange', 'Kraken'), ('Symbol', 'ETHUSD'), ('Total Liquidated Amount', 11),
                      ('Average Liquidated Price', 655), ('Total Liquidated Value', 78150)])
            ]
        )
        self._writer.update_management_overview_sheet()
        print('success')
        self._writer.start = temp_start

    def test_update_sheets(self):
        ss = self._writer.spreadsheet
        # make longer time interval
        self._writer.initiate_default_sheets()
        self._writer.update_sheets()

    def test_open_and_update_sheets(self):
        ss = self._writer.api.open_spreadsheet('1dtLD3OSqfKGvTDTNKatZdE4SuJyDmssgGu1iDdFHSuk')
        self._writer.name += ' (1)'
        # make longer time interval
        temp_start = self._writer.start
        self._writer.start = self._writer.end - datetime.timedelta(days=10)
        self._writer.update_sheets()
        self._writer.start = temp_start

    def test_write_state_to_range(self):
        ss = self._writer.api.open_spreadsheet('1dtLD3OSqfKGvTDTNKatZdE4SuJyDmssgGu1iDdFHSuk')
        self._writer.name += ' (1)'
        updating_fields = [['Updating ...', "'"+str(self._writer.start)], ['', "'"+str(self._writer.end)]]
        success_fields = [['Sheet Updated !', "'"+str(self._writer.start)], ['', "'"+str(self._writer.end)]]

        self._writer._write_state_to_range('Trades', 'D1:E2', updating_fields, 'updating')
        self._writer._write_state_to_range('Withdrawal', 'D1:E2', success_fields, 'success')

    def test_download_and_send_email(self):
        ss = self._writer.api.open_spreadsheet('1dtLD3OSqfKGvTDTNKatZdE4SuJyDmssgGu1iDdFHSuk')
        self._writer.name += ' (1)'
        self._writer.download_report_and_send_email()

if __name__ == '__main__':
    unittest.main()
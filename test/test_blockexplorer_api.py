import unittest
from blockchain_apis.blockexplorer_api import BlockCypherApi, BlockExplorerApi, ETCChainApi, BlockexplorerApis, \
                                            EtherScanApi, Transaction, GasTrackerApi
import datetime


class ApisBaseTest(unittest.TestCase):

    def check_standard_address_details(self, response):
        self.assertIsInstance(response, list)
        if response:
            self.assertIsInstance(response[0], Transaction)

    def tearDown(self):
        del self._api


class TestBlockCypherApi(ApisBaseTest):

    def setUp(self):
        self._api = BlockCypherApi()

    def test_address_details(self):
        btc_address_segwit = self._api.get_address_details('3G2zkVDVJLWymX9HM5ja5sLzEwUETddWdt')
        self.check_standard_address_details(btc_address_segwit)
        # print btc_address_segwit

        btc_address_2 = self._api.get_address_details('1CK6KHY6MHgYvmRQ4PAafKYDrg1ejbH1cE')
        self.check_standard_address_details(btc_address_2)
        # print btc_address_2

        eth_address = self._api.get_address_details('738d145faabb1e00cf5a017588a9c0f998318012', coin_symbol='eth')
        self.check_standard_address_details(eth_address)
        # print eth_address

        dash_address = self._api.get_address_details('Xico5nigvR8Kk2PQZuthSb5dETUf5oAj8g', coin_symbol='dash')
        self.check_standard_address_details(dash_address)
        # print dash_address


class TestBlockExplorerApi(ApisBaseTest):
    """
    BlockExplorer provide the same information as Blockcypher for a couple of coins
    """
    def setUp(self):
        self._api = BlockExplorerApi()

    def test_blockexplorer_api(self):
        btc_address_segwit = self._api.get_address_details('3G2zkVDVJLWymX9HM5ja5sLzEwUETddWdt')
        self.check_standard_address_details(btc_address_segwit)
        # print btc_address_segwit

        btc_address_2 = self._api.get_address_details('12yGv8jG6sMojrLtC4FTDBW969m93ovA1E')
        self.check_standard_address_details(btc_address_2)
        # print btc_address_2

        bch_address = self._api.get_address_details('17aQkRBdnEVLC5N9uAeN3d6dVYyjhLqHhx', coin_symbol='bch')
        self.check_standard_address_details(bch_address)
        # print bch_address

        bch_address_2 = self._api.get_address_details('1NKMFrhXsjBymFucdnG7fWBaVpBCZ9MUsL', coin_symbol='bch', after=datetime.datetime(2017, 11, 1, 0))
        self.check_standard_address_details(bch_address_2)
        # print bch_address_2

        zec_address = self._api.get_address_details('t1c1GCTZXXJ7Eow4nbVEjKoJ6XjcAitt2FV', coin_symbol='zec')
        self.check_standard_address_details(zec_address)
        # print zec_address


class TestEtherScanApi(ApisBaseTest):
    """
    check transactions only for ethereum
    """
    def setUp(self):
        self._api = EtherScanApi()

    def test_etherscan_api(self):
        eth_address = self._api.get_address_details(address='0x3Ff39a7553F0D516Ab1418db6647946292D6E4f5', last_block=4669989, after=datetime.datetime(2017, 11, 22, 0))
        self.check_standard_address_details(eth_address)
        print(eth_address)


class TestETCChainApi(ApisBaseTest):
    """
    check transactions only for ethereum classic
    """
    def setUp(self):
        self._api = ETCChainApi()

    def test_etcchain_api(self):
        etc_address = self._api.get_address_details('0x8907900cfa4e6a8fad333eddbbe183f58b2ee150', after=datetime.datetime(2017, 11, 22, 0))
        self.check_standard_address_details(etc_address)
        # print etc_address


class TestGasTrackerApi(ApisBaseTest):
    """
    check transactions only for ethereum classic
    """
    def setUp(self):
        self._api = GasTrackerApi()

    def test_etcchain_api(self):
        etc_address = self._api.get_address_transactions('0x8907900cfa4e6a8fad333eddbbe183f58b2ee150', after=datetime.datetime(2017, 11, 22, 0))
        self.check_standard_address_details(etc_address)
        # print etc_address


class TestBlockExplorerApis(ApisBaseTest):
    """
    collect transactions from all previous classes. The only class called during recording
    """
    def setUp(self):
        self._api = BlockexplorerApis()

    def test_blockexplorer_apis(self):
        #1AcWJvfMwMtzprqNqsLibhXcNn8A8aurms
        addresses = [
            ('3G2zkVDVJLWymX9HM5ja5sLzEwUETddWdt', 'btc', "bitcoin segwit address"),
            ('1GoFT9LHEsJ7nqZByMtAPfh7WkA82cCuR7', 'btc', "bitcoin legacy address"),
            ('17aQkRBdnEVLC5N9uAeN3d6dVYyjhLqHhx', 'bch', 'bitcoin cash'),
            ('1NKMFrhXsjBymFucdnG7fWBaVpBCZ9MUsL', 'bch', 'bitcoin cash'),
            ('t1NhuVdkXNyDBTEtN2av7fAXqe7D9FJKdcr', 'zec', 'ZCASH address'),
            ('0x3Ff39a7553F0D516Ab1418db6647946292D6E4f5', 'eth', 'ethereum address'),
            ('Xico5nigvR8Kk2PQZuthSb5dETUf5oAj8g', 'dash', 'dash address'),
            ('0x8907900cfa4e6a8fad333eddbbe183f58b2ee150', 'etc', 'ethereum classic address')
        ]

        for addr, cur, comment in addresses:
            response = self._api.get_address_details(addr, cur)
            self.check_standard_address_details(response)

    def test_get_balance(self):
        addresses = [
            # ('3G2zkVDVJLWymX9HM5ja5sLzEwUETddWdt', 'btc', "bitcoin segwit address"),
            # ('1GoFT9LHEsJ7nqZByMtAPfh7WkA82cCuR7', 'btc', "bitcoin legacy address"),
            # ('17aQkRBdnEVLC5N9uAeN3d6dVYyjhLqHhx', 'bch', 'bitcoin cash'),
            # ('1NKMFrhXsjBymFucdnG7fWBaVpBCZ9MUsL', 'bch', 'bitcoin cash'),
            ('t1NhuVdkXNyDBTEtN2av7fAXqe7D9FJKdcr', 'zec', 'ZCASH address'),
            ('0x3Ff39a7553F0D516Ab1418db6647946292D6E4f5', 'eth', 'ethereum address'),
            # ('Xico5nigvR8Kk2PQZuthSb5dETUf5oAj8g', 'dash', 'dash address'),
            ('0x8907900cfa4e6a8fad333eddbbe183f58b2ee150', 'etc', 'ethereum classic address')
        ]
        for addr, cur, comment in addresses:
            response = self._api.get_address_balance(addr, cur)
            self.assertIsInstance(response, float)


if __name__ == '__main__':
    unittest.main()
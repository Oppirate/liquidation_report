import unittest
from unittest import mock
from rest_api import rest
import json
import time
from werkzeug.contrib.cache import MemcachedCache


class TestRestApi(unittest.TestCase):

    def setUp(self):
        rest.app.testing = True
        self.app = rest.app.test_client()

    def test_update_spreadsheet(self):
        data = dict(user_secret='EFxIoYqSSOqE+S18Mv8O0Q5vO5e+YEF/pMBRgU/OKumGD8ohmhdC5LwrLoAMOpE2HV5Ou1UcTYG7ok1Y4hrCx23MyHwb+00xvAcqyHYoeIb58k8fMRNOJZTaX17nUzTF',
                    spreadsheet_key='1dtLD3OSqfKGvTDTNKatZdE4SuJyDmssgGu1iDdFHSuk', date_start='2018-05-14', date_end='2018-05-19', command='update, send')
        rv = self.app.post('/update_spreadsheet', data=data)
        self.assertEqual(rv.status_code, 200)
        data = json.loads(rv.data.decode('utf8'))
        self.assertIsInstance(data, dict)

    def test_weekly_report(self):
        data = dict(spreadsheet_key='1377d2xIoPYHfx1U_Wj7A4Qgc-DSsjn9ZH6TP8JKkpRw', receiver_list=["seifallah.kouki@genesis-mining.com"])
        rv = self.app.post('/weekly_report', data=data)
        self.assertEqual(rv.status_code, 200)
        data = json.loads(rv.data.decode('utf8'))
        self.assertIsInstance(data, dict)

    @mock.patch('rest_api.rest.ReportWriter')
    def test_rest_caching(self, report_writer_mock):
        rest.app.testing = False
        report_writer_mock.update_sheets = mock.Mock(side_effect=lambda: time.sleep(5))
        post_data = dict(spreadsheet_key='1377d2xIoPYHfx1U_Wj7A4Qgc-DSsjn9ZH6TP8JKkpRw', receiver_list=[])
        rv = self.app.post('/weekly_report', data=post_data)
        self.assertEqual(rv.status_code, 200)
        data = json.loads(rv.data.decode('utf8'))
        self.assertIsInstance(data, dict)

        cache = MemcachedCache(['127.0.0.1:11211'])
        current_cache_value = cache.get(post_data['spreadsheet_key'])
        self.assertIsInstance(current_cache_value, dict)
        self.assertEqual(current_cache_value.get('state'), 'done')
        rest.set_cache(post_data['spreadsheet_key'], 'updating')
        rv = self.app.post('/weekly_report', data=post_data)
        self.assertEqual(rv.status_code, 400)
        data = json.loads(rv.data.decode('utf8'))
        self.assertEqual(data['status'], 'error')


if __name__ == '__main__':
    unittest.main()
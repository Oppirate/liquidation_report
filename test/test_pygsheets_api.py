from spreadsheet.pygsheets_api import GoogleSheet
import unittest
import pprint


class TestGoogleSheet(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls._instance = GoogleSheet.instance()

    def test_sheet(self):
        ss = self._instance.spreadsheet('test')
        self.assertEqual(ss.title, 'test')
        ci_sheet = self._instance.create_sheet('Coin Inventory')
        self._instance.delete_sheet1()
        pp = pprint.PrettyPrinter()
        pp.pprint(ci_sheet)

        self._instance.write_table_from_list_of_list(ci_sheet, 'F1:G2', [['Date Begin', 'Date End'], ['2018-04-05', '2018-04-01']])
        pp.pprint(ci_sheet.range('F1:G2'))

    def test_open_spreadsheet(self):
        spreadshett_key = '1377d2xIoPYHfx1U_Wj7A4Qgc-DSsjn9ZH6TP8JKkpRw'
        ss = self._instance.open_spreadsheet(spreadshett_key)
        print(ss.title)
        ws = ss.worksheet(0)
        print(ws)

    def test_update_range(self):
        spreadshett_key = '1377d2xIoPYHfx1U_Wj7A4Qgc-DSsjn9ZH6TP8JKkpRw'
        ss = self._instance.open_spreadsheet(spreadshett_key)
        print(ss.title)
        ws = ss.worksheet(0)
        values = [['18', '2018-04-05 10:59:43', '0xd54344f09d58a7ef0edb7aeea1af43f9cf76676bdd03800b62f388b2c4b764c7', '0x5160bf5CcEa98503534E992bFca42061051cf70A', 'ETH', '2.9851924'],
                  ['19', '2018-04-05 10:59:43', '0xd54344f09d58a7ef0edb7aeea1af43f9cf76676bdd03800b62f388b2c4b764c7', '0x5160bf5CcEa98503534E992bFca42061051cf70A', 'ETH', '2.9851924'],
                  ['20', '2018-04-05 10:59:43', '0xd54344f09d58a7ef0edb7aeea1af43f9cf76676bdd03800b62f388b2c4b764c7',
                   '0x5160bf5CcEa98503534E992bFca42061051cf70A', 'ETH', '2.9851924']]

        self._instance.update_range(ws, 'coins_sent_to_ledger_range', values)
        print(ws)

    def _check_range_length(self, sheet, rng_name, expected_length, old_rng):
        sheet.spreadsheet.update_properties()
        rng = sheet.get_named_range(rng_name)
        # rng.update_named_range()
        s, e = rng.range.split(':')
        cell_s = s[0], int(s[1:])
        cell_e = e[0], int(e[1:])
        self.assertEqual(cell_e[1] - cell_s[1] + 1, expected_length)
        self.assertEqual(old_rng, [cell_s[0], cell_e[0]])

    def test_update_ranges(self):
        spreadshett_key = '1377d2xIoPYHfx1U_Wj7A4Qgc-DSsjn9ZH6TP8JKkpRw'
        ss = self._instance.open_spreadsheet(spreadshett_key)
        print(ss.title)
        ws = ss.worksheet_by_title('Withdrawal')
        values = [['1', 'Bitfinex', '2018-04-05 10:59:43', '0xd54344f09d58a7ef0edb7aeea1af43f9cf76676bdd03800b62f388b2c4b764c7', '100000', '29.851924', 'balbalbla', ''],
                  ['2', 'Bitfinex', '2018-04-06 10:59:43', 'Bank of Montreal', '15000', '29.851924',
                   'balbalbla', ''],
                  ['3', 'Bitfinex', '2018-04-07 10:59:43', 'Bank AG', '80000', '29.851924', 'balbalbla', ''],
                  ['4', 'Bitfinex', '2018-04-08 10:59:43', 'Bank AT', '50000', '69.851924', 'balbalbla', ''],
                  ['5', 'Bitfinex', '2018-04-08 12:59:43', 'Bank AT', '50000', '29.851924', 'balbalbla', ''],
                  ['6', 'Bitfinex', '2018-04-08 12:59:43', 'Bank AT', '50000', '29.851924', 'balbalbla', ''],
                  ['4', 'Bitfinex', '2018-04-08 10:59:43', 'Bank AT', '50000', '69.851924', 'balbalbla', ''],
                  ['5', 'Bitfinex', '2018-04-08 12:59:43', 'Bank AT', '50000', '29.851924', 'balbalbla', ''],
                  ['6', 'Bitfinex', '2018-04-08 12:59:43', 'Bank AT', '50000', '29.851924', 'balbalbla', ''],
                  ['7', 'Bitfinex', '2018-04-08 12:59:43', 'Bank AT', '50000', '29.851924', 'balbalbla', '']
                  ]
        old_rng = [el[0] for el in ws.get_named_range('withdrawals').range.split(':')]
        self._instance.update_range(ws, 'withdrawals', [])
        self._check_range_length(ws, 'withdrawals', 1, old_rng)
        self._instance.update_range(ws, 'withdrawals', values)
        self._check_range_length(ws, 'withdrawals', len(values), old_rng)
        self._instance.update_range(ws, 'withdrawals', values[:3])
        self._check_range_length(ws, 'withdrawals', 3, old_rng)
        self._instance.update_range(ws, 'withdrawals', values)
        self._check_range_length(ws, 'withdrawals', len(values), old_rng)
        print(ws)

    def test_update_range_with_variable_formatting(self):
        spreadshett_key = '1377d2xIoPYHfx1U_Wj7A4Qgc-DSsjn9ZH6TP8JKkpRw'
        ss = self._instance.open_spreadsheet(spreadshett_key)
        print(ss.title)
        ws = ss.worksheet_by_title('Withdrawal')
        values = [['1', 'Bitfinex', '2018-04-05 10:59:43'],
                  ['2', 'Bitfinex', '2018-04-06 10:59:43'],
                  ['3', 'Bitfinex', '2018-04-07 10:59:43'],
                  ['4', 'Bitfinex', '2018-04-08 10:59:43']
                  ]
        # old_rng = [el[0] for el in ws.get_named_range('withdrawals').range.split(':')]
        self._instance.update_ranges(ws, ['total_withdrawals_1', 'total_withdrawals_2'], [])
        # self._check_range_length(ws, 'withdrawals', 1, old_rng)
        self._instance.update_ranges(ws, ['total_withdrawals_1', 'total_withdrawals_2'], values)
        # self._check_range_length(ws, 'withdrawals', len(values), old_rng)
        print(ws)

    def test_batch_delete_insert(self):
        spreadshett_key = '17OGo4U81kTzyNPa3F-tJZNAzzmDuNgg0Nfks09f77fg'
        ss = self._instance.open_spreadsheet(spreadshett_key)
        self._instance.name = 'test'
        print(ss.title)
        ws = ss.worksheet_by_title('Coin Inventory')
        values = [
            ['1', '2018-04-15 22:31:58', '0x0aa089990f17aa5b4ccc8ceb1d090e734c54281a8898fb42367383dacebd8a8c', '0x6190ad253F0C9498Ff156A96db7289a60Ec85f24', 'ETC', '4.31'],
            ['2', '2018-04-15 19:25:32', '0xb98b2fec00fb4859c1611f5381a14c2d95af54a8cee6c7608c813412132a197e', '0x6190ad253F0C9498Ff156A96db7289a60Ec85f24', 'ETC', '4.05'],
            ['3', '2018-04-15 16:14:46', '0x1571430561c0048288642bda1b17efe6e1fa3323ad025d348eb750bc67fbe79f', '0x6190ad253F0C9498Ff156A96db7289a60Ec85f24', 'ETC', '4.31'],
            ['4', '2018-04-15 13:07:58', '0x432b3f4b939e1d45401c4eeb1c629ceadb646bc4160852bd49b204478827863d', '0x6190ad253F0C9498Ff156A96db7289a60Ec85f24', 'ETC', '3.48']
        ]
        values_report = [['ETC', '0x6190ad253F0C9498Ff156A96db7289a60Ec85f24', '76', '311.01'],
                         ['ETH', '0x5160bf5CcEa98503534E992bFca42061051cf70A', '68', '188.43'],
                         ['ZEC', 't1fgPfzu78ZaSRMhMX2XrQUsQeL1r1HHu7n', '0', '0']
                         ]

        ss.batch_start()
        ws.delete_rows(11, 90)
        ws.insert_rows(10, len(values))
        del_ind = 104-90+len(values)
        ws.delete_rows(del_ind, 3)
        ws.insert_rows(del_ind-1, len(values_report))
        ss.batch_stop()
        ss.batch_start()
        ws.update_row(10, values)
        ws.update_row(del_ind, values_report)
        ss.batch_stop()

    # def _get_range_formatting(self, sheet, range_names):
    #     ranges = dict()
    #     min_col = 10000
    #     max_col = 0
    #     for range_name in range_names:
    #         rng = sheet.get_named_range(range_name)
    #         # rng.unlink()
    #         rng_range = rng.range
    #         first_cell, last_cell = rng_range.split(':')
    #         cell = sheet.cell(first_cell).fetch()
    #         row_ind, col_ind = pygsheets.format_addr(first_cell)
    #         last_row_ind, last_col_ind = pygsheets.format_addr(last_cell)
    #         new_last_cell = last_cell[0] + str((len(values) or 1) + row_ind - 1)
    #
    #         ranges[rng_range] = dict(range=rng, cell_format=cell, new_last_cell=new_last_cell)
    #
    #         min_col = min(min_col, col_ind)
    #         max_col = max(max_col, last_col_ind)
    #
    def test_named_Range_with_lanbels(self):
        spreadshett_key = '1377d2xIoPYHfx1U_Wj7A4Qgc-DSsjn9ZH6TP8JKkpRw'
        ss = self._instance.open_spreadsheet(spreadshett_key)
        self._instance.name = 'test'
        print(ss.title)
        ws = ss.worksheet_by_title('Withdrawal')

        values = [['1', 'Bitfinex', '2018-04-05 10:59:43', '0xd54344f09d58a7ef0edb7aeea1af43f9cf76676bdd03800b62f388b2c4b764c7', '100000', '29.851924', 'balbalbla', ''],
                  ['2', 'Bitfinex', '2018-04-06 10:59:43', 'Bank of Montreal', '15000', '29.851924',
                   'balbalbla', ''],
                  ['3', 'Bitfinex', '2018-04-07 10:59:43', 'Bank AG', '80000', '29.851924', 'balbalbla', ''],
                  ['4', 'Bitfinex', '2018-04-08 10:59:43', 'Bank AT', '50000', '69.851924', 'balbalbla', ''],
                  ['5', 'Bitfinex', '2018-04-08 12:59:43', 'Bank AT', '50000', '29.851924', 'balbalbla', ''],
                  ['6', 'Bitfinex', '2018-04-08 12:59:43', 'Bank AT', '50000', '29.851924', 'balbalbla', ''],
                  # ['4', 'Bitfinex', '2018-04-08 10:59:43', 'Bank AT', '50000', '69.851924', 'balbalbla', ''],
                  # ['5', 'Bitfinex', '2018-04-08 12:59:43', 'Bank AT', '50000', '29.851924', 'balbalbla', ''],
                  # ['6', 'Bitfinex', '2018-04-08 12:59:43', 'Bank AT', '50000', '29.851924', 'balbalbla', ''],
                  # ['7', 'Bitfinex', '2018-04-08 12:59:43', 'Bank AT', '50000', '29.851924', 'balbalbla', '']
                  ]

        values_report = [['ETC', '0x6190ad253F0C9498Ff156A96db7289a60Ec85f24', '76'],
                         ['ETH', '0x5160bf5CcEa98503534E992bFca42061051cf70A', '68'],
                         # ['ZEC', 't1fgPfzu78ZaSRMhMX2XrQUsQeL1r1HHu7n', '0']
                         ]

        ss.batch_start()
        ws.delete_rows(5, 10)
        ws.insert_rows(4, len(values))
        del_ind = 4+len(values)+4
        ws.delete_rows(del_ind, 1)
        ws.insert_rows(del_ind-1, len(values_report))
        ss.batch_stop()

        ss.batch_start()
        ws.update_row(5, values)
        ws.update_row(del_ind, values_report)
        ss.batch_stop()

if __name__ == '__main__':
    unittest.main()
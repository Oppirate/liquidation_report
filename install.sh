#!/usr/bin/env bash

sudo apt-get -y install python3-pip memcached libmemcached-dev

pip3 install --user virtualenv
virtualenv -p /usr/bin/python3 venv
source venv/bin/activate
pip3 install --user -r requirements.txt

mkdir ~logs ~logs/errors reports

# run memcached

memcached -d
# Run rest_api

# bash run_report_writer_api.sh
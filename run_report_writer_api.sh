#!/bin/bash

deactivate
pyclean .
killall -mq LiquidationReport.

killall -rq LiquidationReport.

fuser -k 5001/tcp
pkill -f rest_api/rest.py

echo "RESTART Liquidation Report Writer REST API"

source venv/bin/activate
venv/bin/gunicorn -b 0.0.0.0:5001 --reload -w 2 --timeout=600 --access-logfile=gunicorn.log --error-logfile=gunicorn.errors.log -D -n LiquidationReportApi --worker-connections 4 rest_api.rest:app
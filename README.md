# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Generate a liquidation report for HIVE Blockchain Technologies LTD. 

### How it works? ###

- The spreadsheet should be already formatted in the following way:
    
    * It should contains 4 sheets : "Coin Inventory", "Trades", "Withdrawal" and "Management Overview"
    * Each table which has dynamic data should be defined in named range (without headers) according to the format. 
    The script interact with the sheets using well defined named ranges, in this way it is flexible to reformat, redesign the pages as long as the named ranges do not change.
    
        For example, the table trades in the worksheet "Trades", in time of writing, is defined into 2 ranges:
            trades_1: A9:C13 (blue color) and trades_2: D9:K13 (green color).
        The range can change vertically (number of rows) and the colors too however the names should be the same which are hardcoded in the script.

- The script collects data related to each worksheet between two dates using a couple of apis (exchange api, blockexplorer), then it organizes them to fit the tables and update them using the pygsheets api.
- During the update, yellow notifications will appear on the "Management Overview" sheet. The whole update will take more than 100 seconds because it is stopped for almost 25 seconds between each sheet update in a way to avoid reaching google rate limit (100 requests in 100 seconds).
    
    Note: Usually if you trigger the update from the sheet directly, a timeout message from google scripts will appear just ignore it and only follow the yellow notification until it returns to green.
- In case the update fails please make sure to to check that the Named Ranges are set correctly. If an error occurs during the update of a named range, it can cause the table to lose its formatting or the range. 

- The rest api use a caching methodology in a way to prevent updating the same spreadsheet at the same time or in the same rate limit interval (which means preventing updating the same spreadsheet twice in 100 seconds).

- The spreadsheet contain a 5th sheet called control which include two buttons: 
    
    - 'update' is used to update the spreadsheet
    - 'send' Will send email with the spreadsheet (control sheet not included) to a list of email addresses
### How do I get set up? ###
- Clone the repository.

- make sure to add this line to /etc/environment if you are in an EC2 instance:

        LC_ALL="en_US.UTF-8"

- Run bash install.sh to install the dependencies and set up the virtual environment.

- Make sure to set up correctly the needed config files inside ~config folder:
    
    * Generate exchanges api tokens if needed
    * Create a project inside google developers console and generate an oauth web-application file with redirect url to google oauthplayground url and rename it to gmail_client_secret.json. 
    (source: https://stackoverflow.com/questions/19766912/how-do-i-authorise-an-app-web-or-installed-without-user-intervention)
    * From the Oauth2 playground webpage, exchange Authorization code for tokens and put the response into json file called "oauth_playground.json" and set expires_in to 0.
 
 Note: The generated tokens and api keys need to be transfered securely to the server. So they are pushed inside the repository.
- Run the rest api with the bash script:

            $bash run_report_writer_api.sh
### Rest Api ###

The report update is triggered through api calls. That's why a small flask app is implemented with two post calls:

1.  /update_spreadsheet : update the spreadsheet using a secret, its id, a command to be executed and a date range.
    The POST data should look like that:
    
            {   
                "user_secret": "secret",
                "start_date": startValue,
                "end_date": endValue,
                "command": "update",
                "spreadsheet_id": spreadSheetId
             }
    
    - This call is used usually by the formatted spreadsheet directly by clicking on buttons that will trigger a google script that call this api resource with the appropriate command.
    - if no command is passed the spreadsheet will do everything from update, download and send the file.
    
2.  /weekly_report: update the spreadsheet using its id for the last week (between monday and friday including friday), download the report as xlsx fle and share it through email toa list of emails.
    The POST data should look like that:
            
            {
              "spreadsheet_key": "1377d2xIoPYHfx1U_Wj7A4Qgc-DSsjn9ZH6TP8JKkpRw",
              "sender": "sender@email"
              "receiver_list" : []
            }
    - No date range is needed because the last week range (monday -> friday) will be detected automatically
    - This call is triggered by a cron job once a week at Saturday, 00:02 using a curl post call.
    - The curl command will use post data from the json file "~config/weekly_report_client.json" that could be modififed to add email address or to change the spreadsheet. 

## ssh ##
- This project run on the same server with the data collector so to connect ot the server use this command line:

        $ssh -oKexAlgorithms=+diffie-hellman-group1-sha1 ubuntu@ec2-35-156-190-51.eu-central-1.compute.amazonaws.com
        $cd workspace/liquidation_report
- Note: the server can be changed in case it is extended or replaced.


## Config files ##
The config files should look like below:
 
### gmail_client_secret.json ###
- Gmail authentication

    
        {
          "web":{
            "client_id":"client_id.apps.googleusercontent.com",
            "project_id":"hive-report",
            "auth_uri":"https://accounts.google.com/o/oauth2/auth",
            "token_uri":"https://accounts.google.com/o/oauth2/token",
            "auth_provider_x509_cert_url":"https://www.googleapis.com/oauth2/v1/certs",
            "client_secret":"secret",
            "redirect_uris":["https://developers.google.com/oauthplayground"]
          }
        }


### oauth_playgroung.json ###
- Gmail redirect oauth (to prevent manual intervention to allow the api)
-expires_on can be set always to 0.
        
        {
            "access_token": "accccccceeeeeeeeeeeessssss--tooooookkkkkkkeeeeeeeeennnnnnnnn",
            "token_type": "Bearer",
            "expires_in": 0,
            "refresh_token": "refreeeeeeesssssssshhhhh-tooooookkkkkkkkkkkkeeeeeennnnnn"
        }
        
### sheets_api_client_credentials.json ###
-Google Sheets api
-The spreadsheet should be shared with the client_email
        
        {
          "type": "service_account",
          "project_id": "hive-report",
          "private_key_id": "pk_id",
          "private_key": "-----BEGIN PRIVATE KEY-----\nkeykeskeakeakakekkaekak\n-----END PRIVATE KEY-----\n",
          "client_email": "blabla@hive-report.iam.gserviceaccount.com",
          "client_id": "id",
          "auth_uri": "https://accounts.google.com/o/oauth2/auth",
          "token_uri": "https://accounts.google.com/o/oauth2/token",
          "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
          "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/blabla%40hive-report.iam.gserviceaccount.com"
        }
### Future Work ###

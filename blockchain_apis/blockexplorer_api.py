import blockcypher
from blockcypher import constants
import requests
import datetime
import time


class Transaction:
    slots = ('tx_hash', 'time', 'value', 'block_height', 'confirmations', 'balance', 'currency', 'address')

    def __init__(self, **kwargs):
        for k, v in kwargs.items():
            if not k in self.slots:
                continue
            setattr(self, k, v)


class BlockCypherApi:
    COINS = constants.COIN_SYMBOL_LIST + ['dash']

    def __init__(self):
        self._base_url = 'http://api.blockcypher.com/v1/'
        self._maps_coin_value_decimals = {
            'eth': 10**-18,
            'dash': 10**-8
        }

    def perform(self, api, **params):
        kwargs = params.copy()
        if 'txn_limit' in kwargs:
            kwargs['limit'] = kwargs.pop('txn_limit')
        if 'before_bh' in kwargs:
            kwargs['before'] = kwargs.pop('before_bh')
        response = requests.get(self._base_url + api, params=kwargs)
        data = response.json()
        return data

    def convert_transaction(self, tx, coin_symbol):
        if coin_symbol in constants.COIN_SYMBOL_LIST:
            tx['confirmed'] = datetime.datetime.strptime(tx['confirmed'], "%Y-%m-%dT%H:%M:%SZ")
            multp = self._maps_coin_value_decimals[coin_symbol]
            tx['value'] *= multp
            tx['ref_balance'] *= multp
        tx_converted = Transaction(
            tx_hash=tx['tx_hash'],
            time=tx['confirmed'],
            value=tx['value'],
            block_height=tx['block_height'],
            confirmations=tx['confirmations'],
            balance=tx['ref_balance']
        )
        return tx_converted

    def get_address_details(self, address, coin_symbol='btc', after=0, **kwargs):
        if coin_symbol in constants.COIN_SYMBOL_LIST:
            func = blockcypher.get_address_details
            kwargs = dict(address=address, coin_symbol=coin_symbol)
        elif coin_symbol.lower() in ['eth', 'dash']:
            kwargs = dict(api=coin_symbol.lower() + '/main/addrs/' + address)
            func = self.perform
        else:
            raise ValueError('non supported currency')

        details = []
        kwargs['txn_limit'] = 2000
        while True:
            result = func(**kwargs)
            transactions = result.get('txrefs', [])
            if not transactions:
                break
            details += [self.convert_transaction(tx, coin_symbol) for tx in transactions]
            if len(result['txrefs']) < kwargs['txn_limit']:
                break
            if after and details[-1].time < after:
                break
            kwargs['before_bh'] = result['txrefs'][-1]['block_height']
            time.sleep(0.5)
        return details

    def get_address_transactions(self, address, coin_symbol='btc', after=0, **kwargs):
        return self.get_address_details(address, coin_symbol, after, **kwargs)


class BlockExplorerApi:
    COINS = ['btc', 'bch', 'zec']

    def __init__(self):
        self._base_url = 'blockexplorer.com/api/'

    def _perform(self, api, coin_symbol, params=None):
        prefix_map = {
            'zec': 'zcash.',
            'bch': 'bitcoincash.',
            'btc': ''
        }
        url = 'https://' + prefix_map[coin_symbol.lower()] + self._base_url + api
        response = requests.get(url, params=params)
        data = response.json()
        return data

    def get_address_info(self, address, coin_symbol):
        api = 'addr/' + address
        info = self._perform(api, coin_symbol, dict(noTxList=0))
        return info

    def get_address_balance(self, address, coin_symbol):
        res = self.get_address_info(address, coin_symbol)
        return float(res['balance'])

    def convert_transaction(self, tx, address):
        value = 0
        for adr in tx['vin']:
            tx_addr = adr.get('addr') or adr.get('scriptPubKey', {}).get('addresses', [None])[0]
            if not tx_addr:
                continue
            if tx_addr == address:
                value -= float(adr['value'])

        for adr in tx['vout']:
            tx_addr = adr.get('addr') or adr.get('scriptPubKey', {}).get('addresses', [None])[0]
            if not tx_addr:
                continue
            if tx_addr == address:
                value += float(adr['value'])

        return Transaction(
            tx_hash=tx['txid'],
            time=datetime.datetime.utcfromtimestamp(tx['time']),
            value=value,
            block_height=tx['blockheight'],
            confirmations=tx['confirmations'],
            balance=None
        )

    def get_address_batch_transactions(self, address, coin_symbol, fr=0, to=20):
        api = 'addrs/%s/txs/' % address
        params = {
            'from': fr,
            'to': to
        }
        transactions = self._perform(api, coin_symbol, params)
        transactions = [self.convert_transaction(tx, address) for tx in reversed(transactions['items'])]
        return transactions

    def get_address_transactions(self, address, coin_symbol, after=0, **kwargs):
        txs = []
        fr = 0
        batch = 50
        while True:
            result = self.get_address_batch_transactions(address, coin_symbol, fr=fr, to=fr+batch)
            txs += result
            if len(result) < batch:
                break
            if after and after > result[0].time:
                break
            fr += batch
        return txs

    def fill_balances(self, txs, last_balance):
        txs.sort(key=lambda x:x.time, reverse=True)
        current_balance = last_balance
        for tx in txs:
            tx.balance = current_balance
            current_balance -= tx.value
        return txs

    def get_address_details(self, address, coin_symbol='btc', after=0, **kwargs):
        address_info = self.get_address_info(address, coin_symbol)
        txs = []
        fr = 0
        batch = 50
        while True:
            result = self.get_address_batch_transactions(address, coin_symbol, fr=fr, to=fr+batch)
            txs += result
            if len(result) < batch:
                break
            if after and after > result[0].time:
                break
            fr += batch
        txs = self.fill_balances(txs, address_info['balance'])
        return txs


class EtherScanApi:
    COINS = ['eth']

    def __init__(self):
        self._base_url = 'http://api.etherscan.io/'
        self.__key = 'HJHBP6GM1DTM17U6VW32Z863J9156BJYYQ'

    def _perform(self, api, params):
        response = requests.get(self._base_url + api, params=params)
        data = response.json()
        return data

    @staticmethod
    def convert_transaction(tx, address):
        mult = 1
        if tx['from'].lower() == address.lower():
            mult = -1
        return Transaction(
            tx_hash=tx['hash'],
            time=datetime.datetime.utcfromtimestamp(float(tx['timeStamp'])),
            value=int(tx['value']) * mult * 10**-18,
            block_height=int(tx['blockNumber']),
            confirmations=int(tx['confirmations']),
            balance=None
        )

    def get_address_transactions(self, address, coin_symbol='eth', last_block=0, after=0, **kwargs):
        api = 'api'
        endblock = 99999999
        params = dict(
            module='account',
            action='txlist',
            address=address,
            startblock=last_block or 0,
            endblock=endblock,
            apikey=self.__key,
            sort='asc'
        )
        data = self._perform(api, params)
        txs = [self.convert_transaction(tx, address) for tx in data['result'] if int(tx['blockNumber']) > last_block]
        return txs

    def get_address_balance(self, address, coin_symbol='eth'):
        api = 'api'
        params = dict(
            module='account',
            action='balance',
            address=address,
            apikey=self.__key,
            tag='latest'
        )
        data = self._perform(api, params)
        bal = data.get('result')
        if bal:
            bal = float(bal) * 10**-18
        return bal

    def fill_balances(self, txs, last_balance):
        txs.sort(key=lambda x:x.time, reverse=True)
        current_balance = last_balance
        for tx in txs:
            tx.balance = current_balance
            current_balance -= tx.value
        return txs

    def get_address_details(self, address, coin_symbol='ETH', last_block=0, **kwargs):
        balance = self.get_address_balance(address)
        transactions = self.get_address_transactions(address, last_block)
        txs = self.fill_balances(transactions, balance)
        return txs


class GasTrackerApi:
    """
    This is an api client for the hidden Api of Gastracker. It is a limited api because it returns only the last 100 transactions
    the balance is requested from etherhub.io
    """
    COINS = ['etc']

    def __init__(self):
        self._base_url = 'https://api.gastracker.io/v1/'

    def _perform(self, api, params):
        response = requests.get(self._base_url + api, params=params)
        data = response.json()
        return data

    @staticmethod
    def convert_transaction(tx, address):
        mult = 1
        if tx['from'].lower() == address.lower():
            mult = -1
        return Transaction(
            tx_hash=tx['hash'],
            time=datetime.datetime.strptime(tx['timestamp'], '%Y-%m-%dT%H:%M:%S'),
            value=tx['value']['ether']*mult,
            block_height=tx['height'],
            confirmations=tx['confirms'],
            balance=None
        )

    def get_address_balance(self, address, coin_symbol='etc'):
        retries = 3
        for i in range(retries):
            try:
                response = requests.post('http://etherhub.io/web3relay', data={"addr": address, "options": ["balance", "count", "bytecode"]})
                result = response.json()
                return float(result['balance'])
            except Exception as e:
                print(e)

    def get_address_transactions(self, address, coin_symbol='etc', after=0, **kwargs):
        api = 'addr/'+address+'/operations'
        data = []

        result = self._perform(api, dict())
        if not result:
            return result
        result = [self.convert_transaction(res, address) for res in result['items']]

        data += [tx for tx in result if tx.time > after]
        return data


class ETCChainApi:
    """
    This service is not working anymore so it is replaced temporarily with Gas tracker (limited)
    """
    COINS = ['etc']

    def __init__(self):
        self._base_url = 'https://etcchain.com/api/v1/'

    def _perform(self, api, params):
        response = requests.get(self._base_url + api, params=params)
        data = response.json()
        return data

    def get_address_balance(self, address, coin_symbol='etc'):
        api = 'getAddressBalance'
        params = dict(address=address)
        data = self._perform(api, params)
        return data

    @staticmethod
    def convert_transaction(tx, address):
        mult = 1
        if tx['from'].lower() == address.lower():
            mult = -1
        return Transaction(
            tx_hash=tx['hash'],
            time=datetime.datetime.utcfromtimestamp(tx['timestamp']),
            value=tx['valueEther'] * mult,
            block_height=tx['blockNumberLong'],
            confirmations=tx['confirmations'],
            balance=None
        )

    def get_address_transactions(self, address, coin_symbol='etc', after=0, **kwargs):
        api = 'getTransactionsByAddress'
        page = 0
        params = dict(address=address)
        data = []
        while True:
            params['page'] = page
            result = self._perform(api, params)
            if not result:
                break
            result = [self.convert_transaction(res, address) for res in result]
            data += result
            if after and after > result[0].time:
                break
            page += 1
        return data

    def fill_balances(self, txs, last_balance):
        txs.sort(key=lambda x:x.time, reverse=True)
        current_balance = last_balance
        for tx in txs:
            tx.balance = current_balance
            current_balance -= tx.value
        return txs

    def get_address_details(self, address, coin_symbol='etc', after=0, **kwargs):
        info = self.get_address_balance(address)
        txs = self.get_address_transactions(address, after=after)
        txs = self.fill_balances(txs, info['balance'])
        return txs


class BlockexplorerApis:

    def __init__(self):
        self._block_explorer_api = BlockExplorerApi()
        self._block_cypher_api = BlockCypherApi()
        self._etc_chain_api = GasTrackerApi() #ETCChainApi()
        self._etherscan_api = EtherScanApi()

    def get_address_details(self, address, coin_symbol, after=0, **kwargs):
        if coin_symbol.lower() in self._block_cypher_api.COINS:
            api = self._block_cypher_api
        elif coin_symbol.lower() in self._block_explorer_api.COINS:
            api = self._block_explorer_api
        elif coin_symbol.lower() in self._etherscan_api.COINS:
            api = self._etherscan_api
        elif coin_symbol.lower() in self._etc_chain_api.COINS:
            api = self._etc_chain_api
        else:
            raise ValueError('there is no current blockexplorer implemented ofr this currency %s' % coin_symbol)
        return api.get_address_transactions(address, coin_symbol.lower(), after=after, **kwargs)

    def get_address_balance(self, address, coin_symbol):
        if coin_symbol.lower() in self._block_cypher_api.COINS:
            api = self._block_cypher_api
        elif coin_symbol.lower() in self._block_explorer_api.COINS:
            api = self._block_explorer_api
        elif coin_symbol.lower() in self._etherscan_api.COINS:
            api = self._etherscan_api
        elif coin_symbol.lower() in self._etc_chain_api.COINS:
            api = self._etc_chain_api
        else:
            raise ValueError('there is no current blockexplorer implemented ofr this currency %s' % coin_symbol)
        return api.get_address_balance(address, coin_symbol)


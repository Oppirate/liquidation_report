from flask import Flask, request, jsonify
from spreadsheet.report_writer import ReportWriter
from util.lib_util import Util
from util.lib_log import Log
import time
import datetime
from werkzeug.contrib.cache import MemcachedCache

WEEKLY_REPORT_CONFIG_FILE = 'weekly_report_curl.json'

app = Flask(__name__)
cache = MemcachedCache(['127.0.0.1:11211'])
logger = Log(name='RestApi', is_appending=True, folder='')

# link url /ae35b5921998ca5f5f6f462f629f3f8e1eaea9531730dda81c6b298b6e27e8f5


@app.route('/hello_world')
def hello_world():
    return jsonify(dict(status='ok', message='Hello World !!')), 200


def set_cache(sheet_id, state):
    """
    Set memcached cache with the key-value pair (sheet_id, data)
    :param sheet_id:
    :param state:
    :return:
    """
    data = dict(state=state, time=time.time())
    cache.set(sheet_id, data, timeout=100)


def check_spreadsheet_last_time_used_from_cache(sheet_id):
    """
    It checks if a spreadsheet:
        - was recently updated => sleep an offset time to keep almost 100 seconds as time intervals between the updates
        - is updating and reject the update
    :param sheet_id:
    :return:
    """
    state = cache.get(sheet_id)
    if state is None:
        set_cache(sheet_id, 'updating')
        return
    if state['state'] == 'updating':
        raise RuntimeError('The spreadsheet is already updating, please try again later')
    if state['state'] == 'done':
        diff = time.time() - state['time']
        if diff < 100:
            time.sleep(100 - diff)
    return


def update_spreadsheet_with_writer(writer):
    """
    Update sheets inside the spreadsheet
    :param writer:
    :return:
    """
    writer.bootstrap_exchanges()
    writer.update_sheets()
    return writer


def download_and_send_email(writer, receiver_list=None, sender="marc.schurer@logos-fund.com"):
    """
    Download and send email from sender to receiver_list
    :param writer:
    :param receiver_list:
    :param sender:
    :return:
    """
    if not receiver_list:
        config = Util.read_json(Util.get_config_path(WEEKLY_REPORT_CONFIG_FILE))
        receiver_list = config.get('receiver_list', [])
        sender = sender or config.get('sender')
    logger.info('downloading report and sending email')
    writer.download_report_and_send_email(receiver_list, sender)
    logger.info('Email sent !!')


@app.route('/update_spreadsheet', methods=['POST'])
def update_spreadsheet():
    """
    Update spreadsheet by its key.
    The post form should include a dtae range in addition to the spreadsheet key and a user_secret used for authentication
    This call is convenient for js or google scripts.
    It updates the spreadsheet and return a status message if it succeeds
    :return:
    """
    try:
        logger.info('/update_spreadsheet called')
        user_secret = request.form['user_secret']
        if user_secret != 'EFxIoYqSSOqE+S18Mv8O0Q5vO5e+YEF/pMBRgU/OKumGD8ohmhdC5LwrLoAMOpE2HV5Ou1UcTYG7ok1Y4hrCx23MyHwb+00xvAcqyHYoeIb58k8fMRNOJZTaX17nUzTF':
            return jsonify(dict(status='error', message='Unauthorized user')), 401
        spreadsheet_key = request.form['spreadsheet_key']
        date_start = request.form['date_start']
        date_end = request.form['date_end']
        command = request.form.get('command', 'update, send')
        logger.info('Commands: ' + command)
        try:
            date_start = Util.str_to_date(date_start)
        except ValueError:
            date_start = Util.str_to_date(date_start, '%Y-%m-%d')

        try:
            date_end = Util.str_to_date(date_end)
        except ValueError:
            date_end = Util.str_to_date(date_end, '%Y-%m-%d')

        # if app.testing:
        #     logger.info('Testing')
        #     response = dict(status='ok', key=spreadsheet_key, start=date_start, end=date_end)
        #     return jsonify(response), 200

        check_spreadsheet_last_time_used_from_cache(spreadsheet_key)
        logger.info('Initiating report writer')
        writer = ReportWriter(date_start, date_end)
        ss = writer.api.open_spreadsheet(spreadsheet_key)
        writer.name = ss.title
        if 'update' in command.lower():
            logger.info('Updating report')
            writer = update_spreadsheet_with_writer(writer)
        if 'send' in command.lower():
            logger.info('Sending report')
            download_and_send_email(writer)
        set_cache(spreadsheet_key, 'done')
        response = dict(status='ok')
        return jsonify(response), 200
    except Exception as e:
        logger.error(e)
        print(e)
        try:
            set_cache(spreadsheet_key, 'done')
        except:
            pass
        response = dict(status='error', message=str(e))
        return jsonify(response), 400


@app.route('/weekly_report', methods=['POST'])
def weekly_report():
    """
    Update the report for the last week, download it as xlsx file and send it per email with a list of emails (receiver_list).
    To query it using curl: curl --header "Content-Type: application/json" --request POST --data '~config/weekly_report_curl.json' http://localhost:5001/weekly_report
    This call is used inside a crontab job
    :return:
    """
    try:
        logger.info('/weekly_report called')
        if request.form:
            spreadsheet_key = request.form['spreadsheet_key']
            receiver_list = request.form.get('receiver_list', None)
        else:
            data = request.get_json()
            spreadsheet_key = data['spreadsheet_key']
            receiver_list = data.get('receiver_list', None)
        today = datetime.datetime.today()
        saturday_weekday = 6
        if today.isoweekday() == saturday_weekday - 1:
            # friday
            date_end = today.date() + datetime.timedelta(days=1)
        elif today.isoweekday() >= saturday_weekday:  # 5 is saturday isoweekday
            date_end = today.date() - datetime.timedelta(days=today.isoweekday() - saturday_weekday)
        else:
            date_end = today.date() - datetime.timedelta(days=7 - abs(today.isoweekday() - saturday_weekday))

        date_start = date_end - datetime.timedelta(days=5)

        # if app.testing:
        #     logger.info('Testing')
        #     response = dict(status='ok', key=spreadsheet_key, start=date_start, end=date_end, receiver_list=receiver_list)
        #     return jsonify(response), 200

        check_spreadsheet_last_time_used_from_cache(spreadsheet_key)
        writer = ReportWriter(date_start, date_end)
        ss = writer.api.open_spreadsheet(spreadsheet_key)
        writer.name = ss.title
        writer = update_spreadsheet_with_writer(writer)
        download_and_send_email(writer, receiver_list)
        set_cache(spreadsheet_key, 'done')
        response = dict(status='ok')
        return jsonify(response), 200

    except Exception as e:
        logger.error(e)
        print(e)
        try:
            set_cache(spreadsheet_key, 'done')
        except:
            pass
        response = dict(status='error', message=str(e))
        return jsonify(response), 400


if __name__ == '__main__':
    app.debug = True
    app.run()
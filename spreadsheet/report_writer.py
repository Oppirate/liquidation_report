from spreadsheet.pygsheets_api import GoogleSheet
from exchange.bitfinex.bitfinex import Bitfinex
from blockchain_apis.blockexplorer_api import BlockexplorerApis
from util.lib_config import from_file
from util.lib_util import Util
from util.lib_log import Log
import datetime
import time
import os
from collections import OrderedDict
from util.xch_msg import XchOrderMsg

LEDGER_ADDRESSES = {
    'ETH': '0x5160bf5CcEa98503534E992bFca42061051cf70A',
    'ETC': '0x6190ad253F0C9498Ff156A96db7289a60Ec85f24',
    'ZEC': 't1fgPfzu78ZaSRMhMX2XrQUsQeL1r1HHu7n'
}
MAX_CASH_PERMISSIBLE_PER_EXCHANGE = 500000


class ReportWriter:
    """
    Report writer class
    Get data from exchange and blockexplorer apis and update the spreadsheet accordingly
    In this class, a well defined spreadsheet structure is expected.
    Many config is hard coded and can be better managed in the future which are:
        - LEDGER_ADDRESSES that should be checked in the blockexplorer apis
        - MAX_CASH_PERMISSIBLE_PER_EXCHANGE: Value defined by the law
    In case new exchanges are added, they need to be added in the code with bitfinex (bootstrap_exchanges)
    and implement the same exchange calls used in bitfinex
    It is recommended to keep values precision to 2 decimals as it is accepted from regulations
    and is more trusted when checking if values from exchange and ledger matches
    """
    def __init__(self, time_start=None, time_ends=None, report_name='Liquidation Report', ledger_addresses=None):
        self.api = GoogleSheet.instance()
        self.blockexplorer_api = BlockexplorerApis()
        self.name = report_name
        self.start = None
        self.end = None
        self.define_time_interval(time_start, time_ends)
        self._exchanges = dict()
        self._tx_hashs = []
        self._management_overview_reports = dict()
        if ledger_addresses:
            self._ledger_addresses = ledger_addresses
        else:
            self._ledger_addresses = LEDGER_ADDRESSES
        self.log = Log(name='report_writer', is_appending=True, folder='')

    @property
    def exchanges(self):
        if not self._exchanges:
            self.bootstrap_exchanges()
        return self._exchanges

    @property
    def spreadsheet(self):
        return self.api.spreadsheet(self.name)

    def bootstrap_exchanges(self):
        api_config = from_file('api/bitfinex')
        bitfinex = Bitfinex(api_config)
        self._exchanges[bitfinex.exchange_id] = bitfinex

    def define_time_interval(self, start, end):
        if not end:
            end = datetime.datetime.utcnow().date()
        if not start:
            start = end - datetime.timedelta(days=7)
        self.start = start
        self.end = end

    def _round_value(self, value, precision=2):
        return round(value, precision)

    def __initiate_sheet(self, sheet_name):
        sheet = self.api.create_sheet(sheet_name)
        sheet.update_acell('A1', 'Logo')
        sheet.update_acell('B1', sheet_name + ' Report')
        dates = [
            ['Date Begin', str(self.start)],
            ['Date End', str(self.end)]
        ]
        self.api.write_table_from_list_of_list(sheet, 'F1:G2', dates)

    def initiate_default_sheets(self):

        for sheet_name in ['Coin Inventory', 'Trades', 'Withdrawal', 'Management Overview']:
            self.__initiate_sheet(sheet_name)

        self.api.delete_sheet1()

    def _write_cell(self, sheet, msg, column, line):
        field_name = column + str(line)
        self.api.write_field(sheet, field_name, msg)
        line += 2
        return line

    def aggregate_trades_to_orders(self, trades, exchange_id, symbol):
        orders = dict()
        for tr in trades:
            order = orders.get(tr.oid, None)
            if not order:
                order = XchOrderMsg(
                    oid=tr.oid,
                    type=tr.type,
                    timestamp=tr.timestamp,
                    fee_cur_name=tr.fee_cur_name,
                    amount=0,
                    price=0,
                    fee_amount=0,
                    total=0
                )
                orders[order.oid] = order
            order.add_trade(tr)

        result = []
        for oid, oord in orders.items():
            val = OrderedDict([
                ('Trade Nr', oord.oid),
                ('Date', "'" + str(datetime.datetime.utcfromtimestamp(oord.timestamp))),
                ('Exchange', exchange_id),
                ('Symbol', symbol),
                ('Type', oord.type),
                ('Aggregated Amount', abs(oord.amount)),
                ('Average Price', oord.price),
                ('Fee', oord.fee_amount),
                ('Fee Currency', oord.fee_cur_name),
                ('Subtotal', oord.total),
                ('Comment', '')
            ])
            result.append(val)
        return result

    def convert_trades(self, trades, exchange_id, symbol):
        result = []
        for tr in trades:
            val = OrderedDict([
                ('Order ID', tr.tid),
                ('Date', "'" + str(datetime.datetime.utcfromtimestamp(tr.timestamp))),
                ('Exchange', exchange_id),
                ('Symbol', symbol),
                ('Type', tr.type),
                ('Aggregated Amount', self._round_value(abs(tr.amount))),
                ('Average Price', tr.price),
                ('Fee', self._round_value(tr.fee_amount)),
                ('Fee Currency', tr.fee_cur_name),
                ('Subtotal', self._round_value(tr.amount * tr.price)),
                ('Comment', '')
            ])
            result.append(val)
        return result

    def _generate_trades_total_report(self, orders, exchange_id, symbol):
        total_liquidated_amount = sum([o['Aggregated Amount'] for o in orders])
        total_usd = sum([o['Subtotal'] for o in orders])
        try:
            avg_liquidation_price = total_usd / total_liquidated_amount
        except ZeroDivisionError:
            avg_liquidation_price = 0.
        report = OrderedDict([
            ('Exchange', exchange_id),
            ('Symbol', symbol),
            ('Total Liquidated Amount', self._round_value(total_liquidated_amount)),
            ('Average Liquidated Price', avg_liquidation_price),
            ('Total Liquidated Value', self._round_value(total_usd))
        ])
        return report

    def get_exchange_trades(self, exchange):
        """
        get trades from an exchange between start and end times
        :param exchange:
        :return:
        """
        traded_currencies = exchange.get_traded_currencies()
        trades = []
        total_report = []
        for curr in traded_currencies:
            symbol = (curr + 'USD').upper()
            temp_trades = exchange.get_trade_history(curr, 'USD', Util.date_to_timestamp(self.start), Util.date_to_timestamp(self.end))
            orders = self.aggregate_trades_to_orders(temp_trades, exchange.exchange_id, symbol)
            trades += orders
            total_report.append(self._generate_trades_total_report(orders, exchange.exchange_id, symbol))
        return trades, total_report

    def update_trades_sheet(self):
        """
        update trades sheet
        It gets trades from exchanges then it creates table for each of them and report that summarize the total traded
        amount and average price for each market
        some data are saved for the management overview sheet
        :return:
        """
        trades = []
        reports = []
        for xch_id, xch in self.exchanges.items():
            xch_trades, xch_report = self.get_exchange_trades(xch)
            trades += xch_trades
            reports += xch_report

        self._management_overview_reports['trade_reports'] = reports

        trades_sheet = self.api.get_sheet('Trades')
        self.api.update_ranges(trades_sheet, ['trades_1', 'trades_2'], trades, update_sheet=False)
        self.api.update_ranges(trades_sheet, ['trades_report_1', 'trades_report_2'], reports, update_sheet=True)

    def get_exchange_withdrawals(self, exchange):
        """
        get usd withdrawals for an exchange that should have get_deposits_and_withdraws function
        :param exchange:
        :return:
        """
        result = exchange.get_deposits_and_withdraws('USD', Util.date_to_timestamp(self.start), Util.date_to_timestamp(self.end))
        withdrawals = result.get('data', [])

        withds = []
        for withd in withdrawals:
            if withd.type == 'WITHDRAWAL':

                withds.append(OrderedDict([
                    ('Withdrawal ID', withd.transaction_id),
                    ('Exchange', withd.exchange_id),
                    ('Date Sent', "'" + str(datetime.datetime.utcfromtimestamp(withd.timestamp))),
                    ('Benefeciary', withd.target_address),
                    ('Amount in USD', withd.amount),
                    ('Fee in USD', withd.fee),
                    ('Description', withd.description),
                    ('Comment', '')
                ])
                )
        return withds

    def update_withdrawal_sheet(self):
        """
        update withdrawal sheet
        It gets withdrawals from exchanges
        some data are saved for the management overview sheet
        :return:
        """
        withdrawals = []
        reports = []
        for xch_id, xch in self.exchanges.items():
            xch_withdrawals = self.get_exchange_withdrawals(xch)
            withdrawals += xch_withdrawals
            reports.append(OrderedDict([
                ('Exchange', xch_id),
                ('Total Withdrawal Amount Sent', sum([w['Amount in USD'] for w in xch_withdrawals])),
                ('Total Fees', sum([w['Fee in USD'] for w in xch_withdrawals]))
            ]))
        withdrawal_sheet = self.api.get_sheet('Withdrawal')

        self.api.update_ranges(withdrawal_sheet, ['withdrawals_1', 'withdrawals_2'], withdrawals, update_sheet=False)
        self.api.update_ranges(withdrawal_sheet, ['withdrawals_report_1', 'withdrawals_report_2'], reports, update_sheet=True)

    def get_coin_sent_to_ledger(self, coin, address, tx_number_in, tx_number_out):
        start = datetime.datetime.combine(self.start, datetime.datetime.min.time())
        end = datetime.datetime.combine(self.end, datetime.datetime.min.time())

        details = self.blockexplorer_api.get_address_details(address, coin, after=start)
        transactions_in = []
        transactions_out = []
        total_amount_in = 0
        total_amount_out = 0

        nb_tx_in = 0
        nb_tx_out = 0

        for i, tx in enumerate(details):
            if not (start <= tx.time < end):
                continue
            tx_reduced = [
                ('Date', "'" + str(tx.time.replace(microsecond=0))),
                ('Transaction Hash', tx.tx_hash),
                ('Ledger Address', address),
                ('Coin', coin),
                ('Amount', self._round_value(tx.value))
            ]
            if tx.value > 0:
                nb_tx_in += 1
                tx_reduced = OrderedDict([('Transaction Number', tx_number_in + nb_tx_in), *tx_reduced])
                transactions_in.append(tx_reduced)
                total_amount_in += float(tx.value)
            else:
                nb_tx_out += 1
                tx_reduced[-1] = (tx_reduced[-1][0], abs(tx_reduced[-1][1]))
                tx_reduced = OrderedDict([('Transaction Number', tx_number_out + nb_tx_out), *tx_reduced])
                transactions_out.append(tx_reduced)
                total_amount_out += float(tx.value)
            self._tx_hashs.append(tx.tx_hash.lower())
        report_in = OrderedDict([
            ('Coin', coin),
            ('Ledger Address', address),
            ('Number of Transactions', len(transactions_in)),
            ('Total Amount of Coins Received', self._round_value(total_amount_in))
        ])

        report_out = OrderedDict([
            ('Coin', coin),
            ('Ledger Address', address),
            ('Number of Transactions', len(transactions_out)),
            ('Total Amount of Coins Sent', abs(self._round_value(total_amount_out)))
        ])

        return transactions_in, transactions_out, report_in, report_out

    def get_coins_send_to_from_ledger(self):
        coins_sent_to_ledger = []
        reports_in = []
        coins_sent_from_ledger = []
        reports_out = []
        tx_number_in = 0
        tx_number_out = 0

        for coin, address in self._ledger_addresses.items():
            coin_sent_to_ledger, coin_sent_from_ledger, report_in, report_out = self.get_coin_sent_to_ledger(coin, address, tx_number_in, tx_number_out)
            tx_number_in += report_in['Number of Transactions']
            tx_number_out += report_out['Number of Transactions']
            coins_sent_to_ledger += coin_sent_to_ledger
            reports_in.append(report_in)
            coins_sent_from_ledger += coin_sent_from_ledger
            reports_out.append(report_out)

        return coins_sent_to_ledger, reports_in, coins_sent_from_ledger, reports_out

    def get_exchange_deposits(self, exchange, currency, address, previous_txs=0):
        result = exchange.get_deposits_and_withdraws(currency, Util.date_to_timestamp(self.start), Util.date_to_timestamp(self.end))
        deposits = result.get('data', [])

        deps = []
        tx_number = 1
        total_received = 0
        for dep in deposits:
            if dep.type == 'DEPOSIT' and dep.transaction_id.lower() in self._tx_hashs:

                deps.append(OrderedDict([
                    ('Transaction Number', previous_txs + tx_number),
                    ('Exchange', dep.exchange_id),
                    ('Date Sent', "'" + str(datetime.datetime.utcfromtimestamp(dep.timestamp).replace(microsecond=0))),
                    ('Sender Address', dep.target_address),
                    ('Deposit Transaction Hash', dep.transaction_id),
                    ('Coin', currency),
                    ('Amount Received', self._round_value(dep.amount))
                ])
                )
                total_received += dep.amount
                tx_number += 1

        report = OrderedDict([
            ('Exchange', exchange.exchange_id),
            ('Sender Address', address),
            ('Coin', currency),
            ('Number of Transactions', len(deps)),
            ('Total Amount Received', self._round_value(total_received))
        ])
        return deps, report

    def _check_exchange_deposit_and_ledger_sent_reports(self, deposit_report, ledger_reports):
        address_check = False
        consistency = False

        for led_rep in ledger_reports:
            if (led_rep['Coin'] == deposit_report['Coin']) and (led_rep['Ledger Address'] == deposit_report['Sender Address']):
                address_check = True
                if (self._round_value(led_rep['Total Amount of Coins Sent']) == self._round_value(deposit_report['Total Amount Received'])) \
                        and (led_rep['Number of Transactions'] == deposit_report['Number of Transactions']):
                    consistency = True
                break
        deposit_report['Address Check'] = address_check
        deposit_report['Consistency Check'] = consistency
        return deposit_report

    def get_exchange_deposit_reports(self):
        deposits = []
        deposit_reports = []
        tx_numbers = 0
        for xch_id, xch in self.exchanges.items():
            for coin, address in self._ledger_addresses.items():
                xch_deposits, report = self.get_exchange_deposits(xch, coin, address, tx_numbers)
                tx_numbers += len(xch_deposits)
                deposits += xch_deposits
                deposit_reports.append(report)
        return deposits, deposit_reports

    def update_inventory_sheet(self):
        """
        update coins inventory sheet
        It gets transactions from the ledger and deposits from exchanges then it creates table for each of them
        and check if values are correct.
        some data are saved for the management overview sheet
        :return:
        """
        print('Coins Sent to/from Ledger')
        coins_sent_to_ledger, reports_in, coins_sent_from_ledger, reports_out = self.get_coins_send_to_from_ledger()

        print('Exchange deposits')
        deposits, deposit_reports = self.get_exchange_deposit_reports()

        # consistency address checks
        deposit_reports = [self._check_exchange_deposit_and_ledger_sent_reports(dep_rep, reports_out) for dep_rep in deposit_reports]

        self._management_overview_reports['deposit_reports'] = deposit_reports
        self._management_overview_reports['ledger_reports_out'] = reports_out

        coins_sheet = self.api.get_sheet('Coin Inventory')

        for table_name, parts_count, table in [('coins_sent_to_ledger', 2, coins_sent_to_ledger), ('coins_sent_to_ledger_report', 2, reports_in),
                                  ('coins_sent_from_ledger', 2, coins_sent_from_ledger), ('coins_sent_from_ledger_report', 2, reports_out),
                                  ('coins_sent_to_exchanges', 2, deposits), ('coins_sent_to_exchanges_report', 3, deposit_reports)]:
            tables_parts = [table_name + '_' + str(i+1) for i in range(parts_count)]
            update_sheet = (table_name != 'coins_sent_to_ledger')
            self.api.update_ranges(coins_sheet, tables_parts, table, update_sheet=update_sheet)

    def get_ledger_address_balances(self):
        balances = []
        for cur, address in self._ledger_addresses.items():
            balance = self.blockexplorer_api.get_address_balance(address, cur)
            balances.append(OrderedDict([
                ('Coin', cur),
                ('Address', address),
                ('Balance', balance)
            ]))
        return balances

    def get_exchanges_balances(self):
        balances = []
        for xch_id, xch in self.exchanges.items():
            balance = xch.get_wallet_balance()
            balances.append(OrderedDict([
                ('exchange', xch_id),
                ('Total Account Value in USD', balance['total']),
                ('Cash Value in USD', balance['cash']),
                ('Total Coin Holdings Value in USD', balance['coins_value']),
                ('Limit', MAX_CASH_PERMISSIBLE_PER_EXCHANGE)
            ]))
        return balances

    def pd_to_list_of_dicts(self, frame):
        temp_frame = frame.fillna('').reset_index()
        result = temp_frame.values.tolist()
        return result

    def update_management_overview_sheet(self):
        """
        update management overview sheet
        The last sheet to be updated because it depends on data from other sheets
        :return:
        """
        import pandas as pd
        print('management overview')
        ledger_frame = pd.DataFrame(self._management_overview_reports['ledger_reports_out']).groupby('Coin').sum()
        xch_frame = pd.DataFrame(self._management_overview_reports['deposit_reports']).groupby(('Exchange', 'Coin')).sum()

        # get each exchange report into a frame and make a list of them
        frames_xch = [xch_frame[['Total Amount Received']].loc[xch].rename(index=str, columns={'Total Amount Received': xch}) for xch in set(xch_frame.index.get_level_values(0))]
        frames_xch = [ledger_frame[['Total Amount of Coins Sent']].rename(index=str, columns={'Total Amount of Coins Sent': 'Total Amount of Coins Sent to Exchanges'})] + frames_xch
        sent_to_exchanges = pd.concat(frames_xch, axis=1)
        sent_to_exchanges.index.name = 'Coin'

        # add it here manually just for the sheet => should be removed once the exchange is implemented and active
        sent_to_exchanges['Kraken'] = 0
        sent_to_exchanges['Bitmex'] = 0

        consistency_xch = pd.DataFrame(self._management_overview_reports['deposit_reports']).groupby('Coin')[['Number of Transactions']].sum().rename(index=str, columns={'Number of Transactions': 'Number of Successful Received Transactions'})
        consistency_check = pd.concat([ledger_frame[['Number of Transactions']].rename(index=str, columns={'Number of Transactions': 'Number of Successful Sent Transactions'}), consistency_xch], axis=1)

        # limit check
        balances = self.get_exchanges_balances()

        print('performed liquidation')

        hive_balances = self.get_ledger_address_balances()

        management_sheet = self.api.get_sheet('Management Overview')

        for table_name, parts_count, table in [('sent_to_exchanges', 2, self.pd_to_list_of_dicts(sent_to_exchanges)),
                                               ('current_coin_holdings_on_hives_ledger', 2, hive_balances),
                      ('consistency_check', 2, self.pd_to_list_of_dicts(consistency_check)), ('limit_check', 2, balances),
                      ('performed_liquidation', 2, self._management_overview_reports['trade_reports'])]:
            tables_parts = [table_name + '_' + str(i + 1) for i in range(parts_count)]
            update_sheet = (table_name != 'sent_to_exchanges')
            self.api.update_ranges(management_sheet, tables_parts, table, update_sheet=update_sheet)

    def _write_state_to_sheet(self, sheet_name, fields, state):
        sheet = self.api.get_sheet(sheet_name)
        color = (1., 1., 1., 1.)
        if state.lower() == 'updating':
            color = (1., 1., 0, 0.3)
        elif state.lower() == 'success':
            color = (0., 1., 0, 0.3)

        for addr, value in fields.items():
            self.api.write_cell(sheet, addr, value, color)

    def _write_state_to_range(self, sheet_name, rng_name, values, state):
        sheet = self.api.get_sheet(sheet_name)
        color = (1., 1., 1., 1.)
        if state.lower() == 'updating':
            color = (1., 1., 0, 0.3)
        elif state.lower() == 'success':
            color = (0., 1., 0, 0.3)
        self.api.write_range(sheet, rng_name, values, color)

    def update_sheets(self):
        """
        The entry point to update the spreadsheet
        It updates the spreadsheet with state message to show the update progress
        It calls update_<sheet_name>_sheet functions
        It sleeps between updates to prevent google rate limit
        In the end it displays the success green message that disappear in 10 seconds
        :return:
        """
        updating_fields = [['', "'"+str(self.start)], ['', "'"+str(self.end)]]
        success_fields = [['Sheets Updated ! (this message will be removed in 10s)', "'"+str(self.start)], ['', "'"+str(self.end)]]
        clear_fields = [['', ''], ['', '']]

        range_name = 'C1:D2'
        sheet_name = 'Management Overview'

        updating_messages = ['Updating Coin Inventory Sheet ...']
        updating_fields[0][0] = '\n'.join(updating_messages)
        self._write_state_to_range(sheet_name, range_name, updating_fields, 'updating')
        self.update_inventory_sheet()
        time.sleep(25)

        updating_messages = ['Coin Inventory Sheet Updated !', 'Updating Trades Sheet ...']
        updating_fields[0][0] = '\n'.join(updating_messages)
        self._write_state_to_range(sheet_name, range_name, updating_fields, 'updating')
        self.update_trades_sheet()

        time.sleep(25)

        updating_messages.pop(-1)
        updating_messages += ['Trades Sheet Updated !', 'Updating Withdrawal Sheet ...']
        updating_fields[0][0] = '\n'.join(updating_messages)
        self._write_state_to_range(sheet_name, range_name, updating_fields, 'updating')
        self.update_withdrawal_sheet()

        time.sleep(25)

        updating_messages.pop(-1)
        updating_messages += ['Withdrawal Sheet Updated !', 'Updating Management Overview Sheet ...']
        updating_fields[0][0] = '\n'.join(updating_messages)
        self._write_state_to_range(sheet_name, range_name, updating_fields, 'updating')
        self.update_management_overview_sheet()

        self._write_state_to_range(sheet_name, range_name, success_fields, 'success')

        time.sleep(10)

        self._write_state_to_range(sheet_name, range_name, clear_fields, 'clearing')

    def download_report(self):
        """
        Download the report with a custom name indicating the date range
        :return:
        """
        name = self.name + ' %s_%s' % (self.start, self.end)
        self.api.export(name=name)

    def send_email(self, receiver_list, sender, file_name):
        """
        Send an email with attachment from a sender to a receiver_list
        The email text is hard coded in the function but it can be in file in case it changes frequently
        :param receiver_list:
        :param sender:
        :param file_name: The report file path
        :return:
        """
        with open(Util.get_config_path('email.txt')) as f:
            msg = f.read()
        email_message = msg.format(start=self.start, end=self.end)
        response = Util().send_mail(sender, receiver_list, self.name, email_message, file_name)
        print(response)
        self.log.info(response)

    def download_report_and_send_email(self, receiver_list, sender='marc.schurer@logos-fund.com'):
        """
        Download the report with a custom name indicating the date range inside reports folder
        and send it in an email to a list of receivers
        In case the report is downloaded many times it renames one of them and delete the others.
        Each week report is kept inside
        :param receiver_list:
        :return:
        """
        name = self.name.replace(' ', '_').lower() + '_%s_%s' % (self.start, self.end) + 'hive.xlsx'
        dir_path = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
        dir_path = os.path.join(dir_path, 'reports')
        file_path = os.path.join(dir_path, name)
        self.log.info('exporting report')
        self.api.export(name=file_path)
        walk_gen = os.walk(dir_path)
        path, _, files = next(walk_gen)
        for fl in files:
            fl_components = fl.split('.')
            if fl_components[0].endswith('0'):
                new_fl = fl_components[0][:-1] + '.' + fl_components[1]
                os.rename(os.path.join(path, fl), os.path.join(path, new_fl))
            else:
                try:
                    nd = int(fl_components[0][-1])
                    os.remove(os.path.join(path, fl))
                except:
                    pass

        try:
            Util.delete_sheet_from_spreadsheet_file(file_path, 'Control')
        except Exception as e:
            self.log.error(e)

        receiver_list = receiver_list or ['seifallah.kouki@genesis-mining.com', 'seifallah.kouki@blockchain-consulting.net']
        self.log.info('sending email')
        self.send_email(receiver_list, sender=sender, file_name=file_path)

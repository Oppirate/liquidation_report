import pygsheets
from oauth2client.service_account import ServiceAccountCredentials
from util.lib_util import Util


class GoogleSheet:
    """
    Google sheet optimized api
    This class use already the pygsheets
    However it limits its use to deal with just one spreadsheet at a time accessed through spreadsheet function
    """
    __instance = None

    def __init__(self):
        if self.__instance:
            raise RuntimeError('Instance exists already')
        scope = ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive' ]
        creds = ServiceAccountCredentials.from_json_keyfile_name(Util.get_config_path('sheets_api_client_credentials.json'), scope)
        self.client = pygsheets.authorize(credentials=creds)
        self.__spreadsheet = None
        self._max_row = 4
        self._max_col = 0
        self._inbetween_space = 2
        self._batch_requests = []

    def export(self, format=pygsheets.ExportType.MS_Excel, name='liquidation_report'):
        """
        Export the current spreadsheet in a format.
        Note: this usually download the same spreadsheet many times in case it includes many sheets
        :param format:
        :param name:
        :return:
        """
        self.__spreadsheet.export(format, name)

    @property
    def inbetween_space(self):
        return self._inbetween_space

    @property
    def max_row(self):
        return self._max_row

    def spreadsheet(self, name):
        """
        Call a spreadsheet by name.
        If it doesn't exist and no working one is defined then a new one is created with that name
        :param name:
        :return:
        """
        ss = self.__spreadsheet
        if ss is None or ss.title != name:
            ss = self.__create_spreadsheet(name)
        return ss

    def delete_spreadsheet(self):
        if not self.__spreadsheet:
            print('No spreadsheet available')
            return
        self.client.del_spreadsheet(self.__spreadsheet.id)

    def get_sheet(self, name):
        """
        Get a sheet by name from the current spreadsheet
        :param name:
        :return:
        """
        return self.__spreadsheet.worksheet_by_title(name)

    @classmethod
    def instance(cls):
        if not cls.__instance:
            cls.__instance = GoogleSheet()
        return cls.__instance

    def __create_spreadsheet(self, name, email='seifallah.kouki@bcc.gmbh'):
        """
        Create a new spreadsheet and define it as the current working spreadsheet
        :param name:
        :param email:
        :return:
        """
        ss = self.client.create(name)
        ss.share(email, role='writer')
        self.__spreadsheet = ss
        return ss

    def open_spreadsheet(self, ss_id):
        """
        Open spreadsheet by id and define it as the current working spreadsheet
        :param ss_id:
        :return:
        """
        ss = self.client.open_by_key(ss_id)
        self.__spreadsheet = ss
        return ss

    def create_sheet(self, sheet_name, row_count='1000', col_count='26'):
        """
        Create sheet inside the current spreadsheet
        :param sheet_name:
        :param row_count:
        :param col_count:
        :return:
        """
        spreadsheet = self.__spreadsheet
        sheet = spreadsheet.add_worksheet(sheet_name, row_count, col_count)
        return sheet

    def delete_sheet1(self):
        """
        Delete the first working sheet
        Note: it makes to rename it if a new sheet will be created anyway
        :return:
        """
        self.__spreadsheet.del_worksheet(self.__spreadsheet.sheet1)

    def write_table_from_list_of_list(self, sheet, range, values):
        cell_list = sheet.range(range)
        for i, row in enumerate(cell_list):
            for j, val in enumerate(row):
                val.value = values[i][j]
                val.horizondal_alignment = 'CENTER'
                if i == 0:
                    # Header
                    val.text_format['bold'] = True
                val.update()

    def write_field(self, sheet, field_name, field_value):
        cell = sheet.cell(field_name)
        cell.value = field_value
        cell.text_format['bold'] = True
        cell.horizondal_alignment = 'CENTER'
        cell.update()

    def write_cell(self, sheet, cell_addr, field_value, color=None):
        """
        Update cell inside the sheet with the field_value.
        It updates the cell color if it is provided
        :param sheet:
        :param cell_addr:
        :param field_value:
        :param color:
        :return:
        """
        cell = pygsheets.Cell(cell_addr, field_value)
        cell.text_format['bold'] = True
        if color:
            cell.color = color
        cell.link(sheet, update=True)

    def write_range(self, sheet, rng_name, values, cell_color):
        """
        Update range inside the sheet with values.
        Update the range formatting with cell_color
        :param sheet:
        :param rng_name: range format Ex: A1:B20
        :param values:
        :param cell_color:
        :return:
        """
        cell = pygsheets.Cell('A1')
        cell.text_format['bold'] = True
        if cell_color:
            cell.color = cell_color

        rng = sheet.get_values(*rng_name.split(':'), returnas='range')
        sheet.update_cells(rng_name, values)
        rng.apply_format(cell)

    def clear_sheet(self, sheet_name, start, end):
        sheet = self.get_sheet(sheet_name)
        sheet.clear(start, end)

    def link(self, rng):
        rng.link(False)
        rng.update_named_range()

    def _send_batch(self, sheet):
        sheet.client.sh_batch_update(sheet.spreadsheet.id, self._batch_requests, None, sheet.spreadsheet.batch_mode)

    def _update_ranges_format(self, sheet, ranges, batch=False):
        """
        Change format of all cells ranges
        More than one range is updated in one call. This reduces number of requests.
        :param cell: a model :class: Cell whose format will be applied to all cells

        """
        requests = []
        for rng_name, vals in ranges.items():
            rng = vals['range']
            rng.end_addr = vals['new_last_cell']
            # rng.update_values(values)
            # rng.link(False)
            self.link(rng)
            request = {"repeatCell": {
                "range": rng._get_gridrange(),
                "cell": vals['cell_format'].get_json(),
                "fields": "userEnteredFormat,hyperlink,note,textFormatRuns,dataValidation,pivotTable"
                }
            }
            requests.append(request)
        if not batch:
            sheet.client.sh_batch_update(sheet.spreadsheet.id, requests, None, sheet.spreadsheet.batch_mode)
        else:
            self._batch_requests += requests

    def _delete_insert_row_optimized(self, sheet, rows_ind, del_rows_count, values, inherit=False, batch=False):
        """
        delete old rows and insert new ones in the same batch request
        This call is useful to reduce number of requests to the google api which has a rate limit.
        Note: New values are not inserted yet only the rows are prepared to receive the values.
        They are inserted with sheet.update_row call
        :param sheet:
        :param rows_ind:
        :param del_rows_count:
        :param values:
        :param inherit:
        :param batch:
        :return:
        """
        del_rows_ind = rows_ind - 1
        if del_rows_count < 1:
            raise pygsheets.InvalidArgumentValue
        del_request = {'deleteDimension': {'range': {'sheetId': sheet.id, 'dimension': 'ROWS',
                                                 'endIndex': (del_rows_ind + del_rows_count), 'startIndex': del_rows_ind}}}

        number = len(values)
        insert_request = {'insertDimension': {'inheritFromBefore': inherit,
                                       'range': {'sheetId': sheet.id, 'dimension': 'ROWS',
                                                 'endIndex': (del_rows_ind + number), 'startIndex': del_rows_ind}}}
        requests = [del_request, insert_request]
        if not batch:
            sheet.client.sh_batch_update(sheet.spreadsheet.id, requests, None, sheet.spreadsheet.batch_mode)
        else:
            self._batch_requests += requests
        sheet.jsonSheet['properties']['gridProperties']['rowCount'] = sheet.rows + number - del_rows_count

    def update_ranges(self, sheet, range_names, values, update_sheet=True):
        """
        Update a multi-ranges table (table has different formatting defined by ranges)
        The ranges share the same number of rows but different columns.

        :param sheet:
        :param range_names:
        :param values:
        :param update_sheet:
        :return:
        """
        if not range_names:
            return

        if update_sheet:
            sheet.spreadsheet.update_properties(fetch_sheets=False)
        sheet.spreadsheet.batch_start()
        ranges = dict()
        min_col = 10000
        max_col = 0
        for range_name in range_names:
            rng = sheet.get_named_range(range_name)
            # rng.unlink()
            rng_range = rng.range
            first_cell, last_cell = rng_range.split(':')
            cell = sheet.cell(first_cell).fetch()
            row_ind, col_ind = pygsheets.format_addr(first_cell)
            last_row_ind, last_col_ind = pygsheets.format_addr(last_cell)
            new_last_cell = last_cell[0] + str((len(values) or 1) + row_ind - 1)

            ranges[rng_range] = dict(range=rng, cell_format=cell, new_last_cell=new_last_cell)

            min_col = min(min_col, col_ind)
            max_col = max(max_col, last_col_ind)

        if not values:
            row_length = max_col - min_col + 1
            values.append(['']*row_length)
        elif isinstance(values[0], dict):
            values = [list(val.values()) for val in values]

        self._delete_insert_row_optimized(sheet, row_ind, del_rows_count=last_row_ind - row_ind + 1, values=values, batch=False)
        sheet.spreadsheet.batch_stop()
        sheet.update_row(row_ind, values)
        sheet.spreadsheet.batch_start()
        self._update_ranges_format(sheet, ranges, batch=False)
        sheet.spreadsheet.batch_stop()
        # self._send_batch(sheet)
        #
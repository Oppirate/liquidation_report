import inspect
import logging as syslog
from logging.handlers import RotatingFileHandler
import os
import threading
import time

from util.lib_util import Util
# from util.lib_util import env


class LogLevel(object):
    CRITICAL = 10
    FATAL = CRITICAL
    ERROR = 5
    IMPORTANT = 4
    WARNING = 3
    WARN = WARNING
    INFO = 2
    DEBUG = 1
    LOG = 0
    VERBOSE = -3
    _levelNames = {
        CRITICAL: 'CRITICAL',
        ERROR: 'ERROR',
        IMPORTANT: 'IMPORTANT',
        WARNING: 'WARNING',
        INFO: 'INFO',
        DEBUG: 'DEBUG',
        LOG: 'NOTSET',
        VERBOSE: 'VERBOSE',
        'CRITICAL': CRITICAL,
        'ERROR': ERROR,
        'IMPORTANT': IMPORTANT,
        'WARN': WARNING,
        'WARNING': WARNING,
        'INFO': INFO,
        'DEBUG': DEBUG,
        'LOG': LOG,
        'VERBOSE': VERBOSE
    }

    @staticmethod
    def str_to_log_level(log_str):

        try:
            return LogLevel._levelNames.get(log_str.upper(), None)
        except:
            return None


class Log(object):
    """
    Log class
    Used by any processes and can share it with different classes to send output to the same file
    """
    @staticmethod
    def get_instance():
        # type: () -> Log
        if not Log._instance:
            return Log(str(os.getpid()))
        return Log._instance

    _instance = None

    bg_colors = {
        LogLevel.LOG: '\033[95m',
        LogLevel.INFO: '\033[94m',
        LogLevel.DEBUG: '\033[92m',
        LogLevel.IMPORTANT: '\033[1;36;40m',
        LogLevel.WARNING: '\033[93m',
        LogLevel.ERROR: '\033[91m'
    }
    end_color = '\033[0m'

    WEB_SOCKET_LOG_LEVEL = LogLevel.DEBUG

    def __init__(self, name, owner=None, is_appending=False, folder=None, max_byte=None, log_level='DEBUG'):
        pid = os.getpid()

        self.__name = name
        self.__pid = pid
        folder = folder + '/' if folder else ''
        base_dir = '..' if os.getcwd().endswith('/test') else '.'
        if not max_byte:
            self.__handler = syslog.FileHandler('%s/~logs/%s%s.log' % (base_dir, folder, name),
                                            'a' if is_appending else 'w')
        else:
            self.__handler = RotatingFileHandler('%s/~logs/%s%s.log' % (base_dir, folder, name),
                                            'a' if is_appending else 'w', maxBytes=max_byte)
        self.__syslog = syslog.RootLogger(syslog.NOTSET)
        fs = syslog.BASIC_FORMAT
        dfs = None
        fmt = syslog.Formatter(fs, dfs)
        self.__handler.setFormatter(fmt)
        self.__syslog.addHandler(self.__handler)

        # syslog.basicConfig(filename='~logs/%s%s.log' % (folder, name), filemode='a' if is_appending else 'w', level=syslog.NOTSET,
        #                    format='%(threadName)s,%(asctime)s,%(levelname)s:\t%(message)s  (%(filename)s,%(lineno)d)')
        if not max_byte:
            self.__error_file_handler = syslog.FileHandler('%s/~logs/errors/%s.errors.log' % (base_dir, name))
        else:
            self.__error_file_handler = RotatingFileHandler('%s/~logs/errors/%s.errors.log' % (base_dir, name), maxBytes=max_byte)
        self.__error_logger = syslog.Logger('%sErrorLog' % name, level=syslog.ERROR)
        self.__error_logger.addHandler(self.__error_file_handler)

        syslog.info('Start of file')
        self._signal_dispatcher = None
        self._signal_log_message = None
        self._buffer = None
        self._owner = owner
        self.__mutex_log_socket = None
        self.__mutex_log_fn = threading.Lock()
        self.__mutex_buffer = threading.Lock()
        self.__mutex_error = threading.Lock()
        self.__start_time = time.time()
        self.__log_level = LogLevel.INFO
        env_log_level = log_level
        if env_log_level is not False:
            self.__log_level = LogLevel.str_to_log_level(env_log_level)
        self._log_socket = None
        self._time_buffer_started = 0
        self.init()

        Log._instance = self

    @property
    def syslog(self):
        return self.__syslog

    def set_log_socket(self, log_socket):
        self.__mutex_log_socket = threading.Lock()
        self._log_socket = log_socket

    def init(self):

        _gm_time = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())
        welcome_msg = '#### System Started at %s ####' % _gm_time
        self.info(welcome_msg)

        self.__error_logger.error(welcome_msg)
        self.__error_file_handler.flush()

    PREFIXES = {
        LogLevel.CRITICAL: 'FATAL',
        LogLevel.DEBUG: ' DBG ',
        LogLevel.INFO: '  i  ',
        LogLevel.ERROR: 'ERROR',
        LogLevel.WARNING: 'WARN!',
        LogLevel.IMPORTANT: ' !!! '
    }

    def __log(self, _msg, level):
        def __inner():
            try:
                now = time.time()
                pid = os.getpid()
                msg = str(_msg)
                msg = msg.strip(' \t\n\r')
                caller_name = ''
                prefix = self.PREFIXES.get(level, '')
                prefix = '[%5s] ' % prefix
                log_level_offset = 0
                try:
                    local_vars = inspect.currentframe().f_back.f_back.f_back.f_back
                    __caller = None
                    for i in range(0, 5):

                        __caller = local_vars.f_locals['self']
                        if hasattr(__caller, 'name'):
                            caller_name = __caller.name
                        else:
                            caller_class_name = __caller.__class__.__name__
                            caller_name = caller_class_name

                        if caller_name[0] != '_' and caller_name:
                            break
                        local_vars = local_vars.f_back

                    if hasattr(__caller, '__log_level_offset__'):
                        log_level_offset = __caller.__log_level_offset__

                except Exception:
                    caller_name = ''

                delta = now - self.__start_time
                delta = delta or 0
                pid = pid or 0
                thread_id = threading.current_thread()
                lines = str.split(str(msg), '\n')
                message = '%5i %5s %6.2f  %25s > %s %s' % (
                    int(pid), Util.timestamp_to_date(now), float(delta), str(self.__name or caller_name), str(prefix),
                    str(lines[0]))
                if len(lines) > 1:
                    message += '\n'
                    lines = lines[1:]
                    for line in lines:
                        p_line = ' '.ljust(50, ' ') + line
                        message += p_line + '\n'

                self.__print(level, log_level_offset, message)

                if self._log_socket and level - log_level_offset >= self.WEB_SOCKET_LOG_LEVEL:

                    try:
                        with self.__mutex_log_socket:
                            self._log_socket.send_json({
                                'level': level,
                                'message': msg,
                                'caller_name': caller_name,
                                'delta': delta,
                                't': now,
                                'pid': pid
                            })
                        pass
                    except Exception as e:
                        print('FAILED TO SEND LOG!', e)

            except Exception as e:
                print('FAILED TO SEND LOG!', e)

        try:
            # Probably the mutex is not needed here, because we dont really have
            # to guard anything on the inside.
            # self.__mutex_log_fn.acquire()
            __inner()
        finally:
            pass
            # self.__mutex_log_fn.release()

    SYS_LEVELS = {
        LogLevel.CRITICAL: 50,
        LogLevel.ERROR: 40,
        LogLevel.WARNING: 30,
        LogLevel.IMPORTANT: 20,
        LogLevel.INFO: 20,
        LogLevel.DEBUG: 10,
        LogLevel.LOG: 0,
        LogLevel.VERBOSE: 0
    }

    def __print(self, level, log_level_offset, message):
        # sys_level = self.SYS_LEVELS.get(level)
        color = ''
        if level - log_level_offset >= self.__log_level:
            color = self.bg_colors.get(level, self.bg_colors[LogLevel.LOG])

        # if level - log_level_offset >= LogLevel.WARNING or env.LOG_ALL:
        #     print color + message + self.end_color
        #     syslog.info(message)

        # if level - log_level_offset > self.__log_level:
        # Log one more level than the print
        if level - log_level_offset >= self.__log_level:
            print(color + message + self.end_color)
            self.__syslog.log(self.SYS_LEVELS[level], message)

    def buffer(self, msg):
        try:
            self.__mutex_buffer.acquire()
            if self._buffer is None:
                self._buffer = []
                self._time_buffer_started = time.time()
            offset = time.time() - self._time_buffer_started
            self._buffer.append((offset, msg))
        finally:
            self.__mutex_buffer.release()

    def clear_buffer(self):
        with self.__mutex_buffer:
            self._buffer = None

    def get_buffer(self):
        with self.__mutex_buffer:
            return [msg for _, msg in self._buffer]

    def flush_buffer(self, log_level=LogLevel.DEBUG):
        try:
            self.__mutex_buffer.acquire()
            if self._buffer is None:
                return

            msg = '\n\t'.join(['+%4.2f\t%s' % (offset, str(message)) for offset, message in self._buffer])
            self.log(msg, log_level)
            self._buffer = None
            return msg
        finally:
            self.__mutex_buffer.release()

    def verbose(self, msg):
        self.__log(msg, -3)

    def log(self, msg, level=0):
        self.__log(msg, level)

    def important(self, msg, priority=-1):
        self.__log(msg, LogLevel.IMPORTANT)
        title = '[!!!!!] ' + str(msg)[0:32].title()
        text = msg

        self.__send_mail(priority, text, title)

    def fatal(self, msg):
        self.__log(msg, LogLevel.CRITICAL)

    def debug(self, msg):
        self.__log(msg, LogLevel.DEBUG)

    def warning(self, msg):
        self.__log(msg, LogLevel.WARNING)
        # title = '[WARNING] ' + str(msg)[0:32].title()
        # text = str(msg)
        # priority = EmailPriority.WARNING
        # self.__send_mail(priority, text, title)

    def __send_mail(self, priority, text, title):
        # try:
        #     if self._owner and hasattr(self._owner, 'email_service_client'):
        #         client = self._owner.email_service_client
        #     else:
        #         # from lib.util.lib_email_service import EmailServiceClient
        #         #
        #         # client = EmailServiceClient()
        #         client = None
        #     if client:
        #         client.send_mail(title, text, priority)
        # except:
        #     pass
        pass

    def error(self, msg):
        self.__log(msg, LogLevel.ERROR)
        self.__mutex_error.acquire()
        try:
            self.__log(msg, LogLevel.ERROR)

            import traceback

            local_vars = inspect.currentframe().f_back.f_locals
            locals_str = "\n--- Locals ---\n"
            # for key, value in local_vars.items():
            #     try:
            #         locals_str += '%15s = %s \n' % (str(key), str(value))
            #     except:
            #         pass

            if not isinstance(msg, str):
                stack = traceback.format_exc()

                stack_trace = stack + locals_str  # str(msg) + '\n'.join(stack)
                self.__log(stack_trace, LogLevel.ERROR)

                text = stack_trace
                syslog.exception(msg)
                self.__error_logger.exception(msg)
            else:
                text = msg + locals_str
                syslog.error(msg)
                self.__error_logger.error(msg)
            self.__error_file_handler.flush()
            # title = '[ERROR] ' + str(msg)[0:32].title()
            # self.__send_mail(EmailPriority.HIGH, str(msg) + str(text), title)

            pass
        except:
            pass
        finally:
            self.__mutex_error.release()

    def info(self, msg):
        self.__log(str(msg), LogLevel.INFO)

    def log_to_slack(self, msg, emoji=None, username=None, is_forced=False):
        print(username, msg)

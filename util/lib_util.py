
class Util:
    """
    List of utility functions
    """
    @classmethod
    def str_to_date(cls, input_str, date_format='%Y-%m-%d %H:%M:%S'):
        import datetime

        return datetime.datetime.strptime(input_str, date_format)

    @classmethod
    def timestamp_to_date(cls, input_timestamp):
        import datetime

        return datetime.datetime.fromtimestamp(
            int(input_timestamp)
        ).strftime('%Y-%m-%d %H:%M:%S')

    @staticmethod
    def date_to_timestamp(input_date):
        if isinstance(input_date, float) or isinstance(input_date, int):
            return input_date

        import calendar

        return calendar.timegm(input_date.timetuple())

    @staticmethod
    def get_config_path(file_name):
        """
        Return the path of a file inside the config directory
        The config directory is assumed to be in the same folder as the directory of the current file
        :param file_name:
        :return:
        """
        import os
        CONFIG_BASE_PATH = '~config/'
        dir_path = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
        file_path = os.path.join(dir_path, CONFIG_BASE_PATH + file_name)
        return file_path

    @staticmethod
    def delete_sheet_from_spreadsheet_file(spreadsheet_path, sheet_name):
        import openpyxl
        spreadsheet = openpyxl.load_workbook(spreadsheet_path)
        if sheet_name not in spreadsheet.get_sheet_names():
            return
        sheet_del = spreadsheet.get_sheet_by_name(sheet_name)
        spreadsheet.remove(sheet_del)
        spreadsheet.save(spreadsheet_path)

    @staticmethod
    def read_json(path):
        import json
        with open(path, 'r') as f:
            obj = json.loads(f.read())
        return obj

    @staticmethod
    def create_message_with_attachment(sender, to, subject, message_text, file):
        """Create a message for an email.

        Args:
          sender: Email address of the sender.
          to: Email address of the receiver.
          subject: The subject of the email message.
          message_text: The text of the email message.
          file: The path to the file to be attached.

        Returns:
          An object containing a base64url encoded email object.
        """
        from email.mime.multipart import MIMEMultipart
        from email.mime.text import MIMEText
        from email.mime.base import MIMEBase
        import email
        import base64
        import os
        import magic
        message = MIMEMultipart()
        message['to'] = to
        message['from'] = sender
        message['subject'] = subject
        message['Date'] = email.header.Header(email.utils.formatdate())
        message['Message-ID'] = email.header.Header(email.utils.make_msgid())

        msg = MIMEText(message_text)
        message.attach(msg)

        filetype = magic.from_file(file, mime=True)
        maintype, subtype = filetype.split('/')
        part = MIMEBase(maintype, subtype)
        part.set_payload(open(file, 'rb').read())
        filename = os.path.basename(file)
        email.encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment; filename="{}"'.format(os.path.basename(filename)))
        message.attach(part)

        return {'raw': base64.urlsafe_b64encode(message.as_bytes()).decode()}

    @staticmethod
    def read_oauth_tokens_from_json_file(path):
        import json
        with open(path) as f:
            content = json.loads(f.read())
        return content

    def send_mail(self, send_from, send_to, subject, text, file_name):
        """
        SEnd mail with text and file attachment
        :param send_from:
        :param send_to:
        :param subject:
        :param text:
        :param file_name:
        :return:
        """
        import httplib2
        from apiclient import discovery
        from oauth2client.client import flow_from_clientsecrets, OAuth2Credentials
        from oauth2client.file import Storage
        import datetime

        # Path to the client_secret.json file downloaded from the Developer Console
        CLIENT_SECRET_FILE = Util.get_config_path('gmail_client_secret.json')

        # Check https://developers.google.com/gmail/api/auth/scopes for all available scopes
        SCOPES = ['https://www.googleapis.com/auth/gmail.compose',
                  'https://www.googleapis.com/auth/gmail.send'
                  ]

        # redirect url
        REDIRECT_URI = 'https://developers.google.com/oauthplayground'
        # Location of the credentials storage file
        STORAGE = Storage('gmail.storage')

        # Start the OAuth flow to retrieve credentials
        flow = flow_from_clientsecrets(CLIENT_SECRET_FILE, scope=SCOPES)
        flow.redirect_uri = REDIRECT_URI
        flow.user_agent = 'google-oauth-playground'

        # Try to retrieve credentials from storage or run the flow to generate them
        credentials = STORAGE.get()
        if credentials is None or credentials.invalid:
            oauth_tokens = self.read_oauth_tokens_from_json_file(Util.get_config_path('oauth_playground.json'))
            delta = datetime.timedelta(seconds=int(oauth_tokens['expires_in']))
            token_expiry = delta + datetime.datetime.utcnow()
            credentials = OAuth2Credentials(
                oauth_tokens['access_token'], flow.client_id, flow.client_secret,
                oauth_tokens['refresh_token'], token_expiry, flow.token_uri, flow.user_agent,
                revoke_uri=flow.revoke_uri,
                token_response=oauth_tokens, scopes=flow.scope,
                token_info_uri=flow.token_info_uri)

        # Authorize the httplib2.Http object with our credentials
        http = httplib2.Http()
        http = credentials.authorize(http)

        # Build the Gmail service from discovery
        gmail_service = discovery.build('gmail', 'v1', http=http, cache_discovery=False)

        # create a message to send
        if isinstance(send_to, list):
            send_to = ', '.join(send_to)
        body = self.create_message_with_attachment(send_from, send_to, subject, text, file_name)

        # send it
        try:
            message = (gmail_service.users().messages().send(userId="me", body=body).execute())
            print('Message Id: %s' % message['id'])
            print(message)
            return dict(status='ok',  message=message)
        except Exception as error:
            print('An error occurred: %s' % error)
            return dict(status='error', message=error)

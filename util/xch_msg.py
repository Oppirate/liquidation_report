import json


class XchMsg(object):
    """
    Message format base class
    Messages are used mainly by the exchange classes that have different response format which makes sense
    to convert them to unique message structure.
    """
    in_model = None
    __slots__ = 'status',
    __types__ = {}

    def __init__(self, **kwargs):
        self.status = 'ok'
        if kwargs:
            # if self.in_model:
            # Util.check_form_with_print(self.in_model, kwargs, None)
            for slot in self.__slots__:
                value = kwargs.get(slot, None)
                custom_type = self.__types__.get(slot)
                if custom_type:
                    value = custom_type(value)
                setattr(self, slot, value)

    def __getitem__(self, item):
        item = str(item)
        if 'data' not in self.__slots__:
            if item == 'data':
                return self.to_dict()
        # try:
        return getattr(self, item)
        # except AttributeError as e:
        #     # logging.error(self.to_dict())
        #     raise AttributeError()

    def __setitem__(self, key, value):
        setattr(self, key, value)

    def __contains__(self, item):
        return self.has_key(item)

    def __str__(self):
        return str(self.to_str())

    def __repr__(self):
        return self.to_str()

    def to_str(self):
        return json.dumps(self.to_dict())

    def has_key(self, item):
        return item in self.__slots__ and getattr(self, item, None) is not None

    def to_dict(self):
        result = {}
        for key in self.__slots__:
            value = getattr(self, key, None)
            if isinstance(value, XchMsg):
                value = value.to_dict()
            result[key] = value
        return result

    def get(self, item, default=None):
        try:
            return self[item]
        except:
            return default


class XchTransactionMsg(XchMsg):
    __slots__ = ('tid', 'oid', 'amount', 'price', 'type', 'timestamp', 'fee_cur_name', 'fee_amount')


class XchOrderMsg(XchMsg):
    __slots__ = ('status', 'oid', 'amount', 'price', 'type', 'timestamp', 'amount', 'fee_amount', 'fee_cur_name', 'total')

    def add_trade(self, trade):
        if trade.oid != self.oid or trade.type != self.type or trade.fee_cur_name != self.fee_cur_name:
            return
        self.amount += trade.amount
        self.total += trade.amount * trade.price
        self.fee_amount += trade.fee_amount
        self.price = self.total / self.amount


class XchMovementMsg(XchMsg):
    __slots__ = ('transaction_id', 'target_address', 'currency', 'exchange_id', 'amount', 'type', 'description', 'timestamp', 'id', 'fee')
import ujson


def obj_to_dict(obj):
    result = None
    if obj is None:
        return
    if hasattr(obj, 'to_dict'):
        result = obj.to_dict()
    elif isinstance(obj, dict):
        result = {}
        for key in obj:
            res = obj_to_dict(obj[key])
            if res is not None:
                result[key] = res
    elif isinstance(obj, list):
        result = []
        for entry in obj:
            res = obj_to_dict(entry)
            if res is not None:
                result.append(res)
    elif isinstance(obj, str) or isinstance(obj, int) or isinstance(obj, float):
        result = obj

    return result


def read_file(file_name, base=''):
    file_path = '%s~config/%s.json' % (base, file_name)
    try:

        import os
        abs_path = os.path.abspath(file_path)
        # print 'Open ' + abs_path
        f = open(abs_path, 'r')
        data = ujson.load(f)
        f.close()
        return data
    except IOError as e:
        if base == '':
            return read_file(file_name, '../')
        print('ERROR')
        import os
        print('base dir: ', os.path.realpath(os.curdir))
        print(e)
        return None


def from_file(file_name):
    """
    return a config file from a file_name
    :param file_name:
    :return:
    """
    data = read_file(file_name)
    base_path = '/'.join(file_name.split('/')[:-1])
    return read_data(data, base_path)


def read_data(data, base_path):
    """
    Read data from file and convert it to a config class
    :param data:
    :param base_path:
    :return:
    """
    if '__file_name__' in data:
        file_name = data['__file_name__']
        file_data = read_file(base_path + '/' + file_name)
        file_data.update(data)
        data = file_data
        data['__file_name__'] = file_name
        data['__base_path'] = base_path

    if '__class__' in data:
        class_name = data['__class__']
        constructor = globals()[class_name]
        k_data = data.copy()
        del k_data['__class__']
        k_data['__base_path'] = base_path
        value = constructor(**k_data)
        return value


class ConfigClass(object):
    __private_slots__ = ()
    __default_values__ = {}

    def is_default_value(self, attr_name):
        if attr_name not in self.__default_values__:
            return False
        return getattr(self, attr_name, None) == self.__default_values__.get(attr_name, None)

    def __init__(self, **kwargs):
        self.__file_name__ = None
        self.__base_path__ = kwargs.get('__base_path')
        self.from_dict(kwargs)

        self._on_init(**kwargs)

    def _on_init(self, **kwargs):
        pass

    def from_dict(self, data):

        all_members = [item for item in self.__slots__]
        all_members.extend([item for item in self.__private_slots__])
        self.__file_name__ = data.get('__file_name__', None)
        for key in all_members:

            try:
                value = data[key]
            except KeyError:
                if getattr(self, key, None) is None:
                    value = self.__default_values__.get(key, None)
                    if value is not None:
                        setattr(self, key, value)
                continue
            if isinstance(value, list):
                value = [read_data(entry, self.__base_path__) for entry in list(value)]
            elif isinstance(value, dict):
                value = read_data(value, self.__base_path__)

            setattr(self, key, value)

    def to_dict(self):
        result = {'__class__': self.__class__.__name__}
        if self.__file_name__:
            result['__file_name__'] = self.__file_name__
            # return result

        for slot in self.__slots__:
            res = getattr(self, slot, None)
            result[slot] = obj_to_dict(res)

        for slot in self.__private_slots__:
            res = getattr(self, '_' + self.__class__.__name__ + '__' + slot)
            result[slot] = obj_to_dict(res)

        return result


class APIConfig(ConfigClass):
    """
    Api Config describes the API endpoints of an exchange.
    """
    __slots__ = ('user', 'key', 'secret')
    __default_values__ = {
        'user': ''
    }